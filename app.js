const restify = require('restify');
const corsMiddleware = require('restify-cors-middleware');
const mongoose = require('mongoose');
const logger = require('morgan');
const auth = require('./helpers/auth')();
require('colors');

const app = {};

const env = process.env.NODE_ENV || 'dev';
const config = require(`./config/${env}`);

app.server = restify.createServer({
  name: 'websirius',
  version: '1.0.0'
});

const cors = corsMiddleware({
  origins: ['http://localhost:7000'],
  allowHeaders: [
    'Access-Control-Allow-Credentials',
    'Access-Control-Allow-Methods',
    'Access-Control-Expose-Headers',
    'Access-Control-Allow-Origin',
    'Content-Type',
    'accept',
    'authorization'
  ]
});

app.server.use(logger('dev'));
app.server.use(restify.acceptParser(app.server.acceptable));
app.server.use(restify.queryParser());
app.server.use(restify.bodyParser());
app.server.use(auth.initialize());
app.server.pre(cors.preflight);
app.server.pre(cors.actual);

// Routes
const index = require('./routes/index');
const projects = require('./routes/projects');
const session = require('./routes/session');
const users = require('./routes/users');
const events = require('./routes/events');
const news = require('./routes/news');

mongoose.Promise = global.Promise;
global.HOST = config.frontend.url;

if (env !== 'test') {
  mongoose.connect(config.db.url);
  app.db = mongoose.connection;
  app.db.on('open', () => {
    console.log('connected to db'.yellow);
  });
}

index(app, '/api');
session(app, '/api/session');
projects(app, '/api/projects', auth);
users(app, '/api/users', auth);
events(app, '/api/events', auth);
news(app, '/api/news', auth);

module.exports = app;
