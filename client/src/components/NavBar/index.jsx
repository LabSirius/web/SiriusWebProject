// @flow
import React from 'react';
import { Button, Image, Menu } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import logo from './images/logo_sirius.svg';
import navItems from './data/items.json';

type navBarItemType = {
  name: string,
  displayName: string
};

const logoStyle = {
  padding: '10px 0 10px 5px'
};

class NavBar extends React.Component {
  state = {
    activeItem: ''
  };

  handleItemClick = (e: SyntheticEvent, { name }: navBarItemType) =>
    this.setState({ activeItem: name });

  render() {
    const { activeItem } = this.state;

    return (
      <Menu fixed="top">
        <Menu.Header>
          <Image as={Link} to="/" size="mini" src={logo} style={logoStyle} />
        </Menu.Header>
        <Menu.Menu position="right">
          {navItems.map(({ name, displayName }: navBarItemType) => {
            return (
              <Menu.Item
                key={name}
                name={name}
                active={name === activeItem}
                onClick={this.handleItemClick}
                as={Link}
                to={'/' + name}
              >
                {displayName}
              </Menu.Item>
            );
          })}
          <Menu.Item>
            <Button
              color="blue"
              size="mini"
              as={Link}
              name="loginButton"
              to="/login"
            >
              Iniciar sesión
            </Button>
          </Menu.Item>
          <Menu.Item>
            <Button
              color="grey"
              size="mini"
              as={Link}
              name="joinButton"
              to="/join"
            >
              Registro
            </Button>
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  }
}

export default NavBar;
