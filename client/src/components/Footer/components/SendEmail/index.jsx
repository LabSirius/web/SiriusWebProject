// @flow
import React from 'react';
import { Button, Container, Form } from 'semantic-ui-react';

type inputAttr = {
  name: string,
  value: string
};

const validationRegex = {
  name: /[\wáéíóúüñ ]{2,35}/i,
  email: /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,5})+$/,
  comment: /^[\w\d\sáéíóúüñ.,:;\-()"#$%&¡!¿?]{5,500}$/im
};

class SendEmail extends React.Component {
  state = {
    name: {
      value: '',
      valid: true
    },
    email: {
      value: '',
      valid: true
    },
    comment: {
      value: '',
      valid: true
    }
  };

  validateInput = (name: string, value: string) =>
    validationRegex[name].test(value);

  validateForm = () => {
    const name = this.state.name.value;
    const email = this.state.email.value;
    const comment = this.state.comment.value;
    return (
      this.validateInput('name', name) &&
      this.validateInput('email', email) &&
      this.validateInput('comment', comment)
    );
  };

  handleInputChange = (e: SyntheticEvent, { name, value }: inputAttr) => {
    const valid = this.validateInput(name, value);
    this.setState({ [name]: { value, valid } });
  };

  handleSubmit = (e: SyntheticEvent) => {
    // const trimmedValue = value.trim();
    alert(this.validateForm() ? 'TODO: OK' : 'TODO: Not cool');
  };

  render() {
    const nameValue = this.state.name.value;
    const emailValue = this.state.email.value;
    const commentValue = this.state.comment.value;

    const validName = this.state.name.valid;
    const validEmail = this.state.email.valid;
    const validComment = this.state.comment.valid;

    return (
      <Container as={Form} textAlign="center" onSubmit={this.handleSubmit}>
        <Form.Group widths="equal">
          <Form.Input
            error={!validName}
            name="name"
            placeholder="Nombre"
            value={nameValue}
            onChange={this.handleInputChange}
          />
          <Form.Input
            error={!validEmail}
            name="email"
            placeholder="Correo electrónico"
            value={emailValue}
            onChange={this.handleInputChange}
          />
        </Form.Group>
        <Form.TextArea
          error={!validComment}
          name="comment"
          placeholder="Comentario"
          value={commentValue}
          onChange={this.handleInputChange}
        />
        <Button type="submit">Enviar</Button>
      </Container>
    );
  }
}

export default SendEmail;
