// @flow
import React from 'react';
import { List } from 'semantic-ui-react';

const divStyle = {
  margin: '1em 0'
};

function Info({ items }: { items: Array<string> }) {
  return (
    <div style={divStyle}>
      {items.map((s: string, i: number) => <List.Item key={i} content={s} />)}
    </div>
  );
}

export default Info;
