// @flow
import React from 'react';
import { Container, Grid, Icon, Image, Segment } from 'semantic-ui-react';

import Info from './components/Info';
import SendEmail from './components/SendEmail';

import logoWithText from './images/logo_sirius_text.svg';
import {
  directorInfo,
  facultyInfo,
  locationInfo,
  sciDirectorInfo
} from './data/group_info.json';
import links from './data/links.json';

type linkType = { icon: string, href: string };

function Footer(props: {}) {
  return (
    <Segment inverted vertical>
      <Container>
        <Grid inverted divided="vertically">
          <Grid.Row as="h1" centered>
            Contacto
          </Grid.Row>
          <Grid.Row>
            <Grid.Column as="list" mobile={16} tablet={16} computer={4}>
              <Info items={facultyInfo} />
              <Info items={locationInfo} />
            </Grid.Column>
            <Grid.Column as="list" mobile={16} tablet={16} computer={4}>
              <Info items={directorInfo} />
              <Info items={sciDirectorInfo} />
            </Grid.Column>
            <Grid.Column mobile={16} tablet={16} computer={8}>
              <SendEmail />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column mobile={16} tablet={16} computer={8}>
              <Image
                src={logoWithText}
                alt="Grupo de Investigación Sirius"
                size="small"
              />
            </Grid.Column>
            <Grid.Column mobile={16} tablet={16} textAlign="right" computer={8}>
              {links.map(({ icon, href }: linkType, i: number) =>
                <a key={i} target="_blank" href={href}>
                  <Icon link inverted size="big" name={icon} />
                </a>
              )}
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </Segment>
  );
}

export default Footer;
