// @flow
import React from 'react';
import { Container, Grid, Image } from 'semantic-ui-react';

import logoWithText from './images/logo_sirius_text.svg';
import utp from './images/utp.svg';

import Particles from 'react-particles-js';

const bannerStyle = {
  backgroundColor: '#000',
  width: '100%',
  height: '250px'
};
const containerStyle = { paddingTop: '100px' };

class Header extends React.Component {
  render() {
    return (
      <div style={bannerStyle}>
        <Container style={containerStyle}>
          <Grid>
            <Grid.Row only="computer">
              <Particles
                params={{
                  particles: {
                    number: {
                      value: 40
                    },
                    line_linked: {
                      shadow: {
                        enable: true,
                        color: '#3CA9D1',
                        blur: 5
                      },
                      width: 1.5
                    },
                    move: {
                      speed: 4
                    },
                    size: {
                      value: 3
                    }
                  }
                }}
                style={{
                  width: '100%',
                  position: 'absolute',
                  marginTop: -100
                }}
                height={250}
              />
              <Grid.Column mobile={16} tablet={16} computer={8}>
                <Image
                  src={logoWithText}
                  alt="Grupo de Investigación Sirius"
                  size="medium"
                />
              </Grid.Column>
              <Grid.Column computer={8}>
                <Image
                  src={utp}
                  alt="Grupo de Investigación Sirius"
                  size="medium"
                  floated="right"
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row only="mobile tablet">
              <Grid.Column mobile={16} tablet={16} computer={8}>
                <Image
                  src={logoWithText}
                  alt="Grupo de Investigación Sirius"
                  size="medium"
                />
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </div>
    );
  }
}

export default Header;
