import React from 'react';
import { Icon } from 'semantic-ui-react';

import list from './data/list.json';

type listType = { icon: string, name: string, color: string };

class Social extends React.Component {
  render() {
    return (
      <div>
        {list.map(({ icon, name, color }: listType, i: number) =>
          <a key={i}>
            <Icon link size="big" color={color} name={icon} />
          </a>
        )}
      </div>
    );
  }
}

export default Social;
