import React from 'react';
import {
  Grid,
  Icon,
  Image,
  Divider,
  Button,
  Form,
  Message
} from 'semantic-ui-react';

import Social from './src/Social';

import logo from './images/sirius.svg';

class Login extends React.Component {
  render() {
    return (
      <div>
        <div className="login-form">
          <Grid textAlign="center" verticalAlign="middle">
            <Grid.Column style={{ maxWidth: 450 }}>
              <Message
                attached
                header="Iniciar Sesión"
                content="Uso exlusivo para integrantes del Grupo Sirius"
              />
              <Form size="large" className="attached fluid segment">
                <Image size="medium" src={logo} centered />
                <Divider hidden />
                <Social />
                <Divider section />
                <Form.Input
                  fluid
                  icon="user"
                  iconPosition="left"
                  placeholder="Correo Electrónico o Usuario"
                />
                <Form.Input
                  fluid
                  icon="lock"
                  iconPosition="left"
                  placeholder="Contraseña"
                  type="password"
                />
                <Button>INICIAR SESIÓN</Button>
              </Form>
              <Message attached="bottom" warning>
                <Icon name="help" />
                ¿No tienes cuenta? <a href="/join">Registrarse</a>
              </Message>
            </Grid.Column>
          </Grid>
        </div>
      </div>
    );
  }
}

export default Login;
