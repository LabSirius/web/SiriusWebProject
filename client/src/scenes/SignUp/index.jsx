import React from 'react';
import {
  Grid,
  Icon,
  Image,
  Divider,
  Button,
  Form,
  Message
} from 'semantic-ui-react';
import axios from 'axios';

import Social from './src/Social';
import logo from './images/sirius.svg';

class SignUp extends React.Component {
  state = {
    fields: {
      username: '',
      email: '',
      firstName: '',
      lastName: '',
      password: '',
      confirmedPassword: ''
    },
    loading: false,
    success: false,
    errorFlags: {
      username: false,
      email: false,
      firstName: false,
      lastName: false,
      password: false,
      confirmedPassword: false
    },
    errorMessages: []
  };

  handleSubmit = s => {
    this.setState({ loading: true });
    this.checkPassword();

    axios
      .post('/api/users', this.state.fields)
      .then(res => {
        this.setState({
          success: true,
          fields: {
            username: '',
            email: '',
            firstName: '',
            lastName: '',
            password: '',
            confirmedPassword: ''
          }
        });
      })
      .catch(err =>
        this.setState(curState => {
          curState.error = true;
          curState.errorMessages.push('Problemas en el servidor');
        })
      );
    this.setState({ loading: false });
  };

  checkPassword = () => {
    const { password, confirmedPassword } = this.state.fields;
    if (password !== confirmedPassword) {
      this.setState(curState => {
        curState.error = true;
        curState.errorFlags.password = true;
        curState.errorFlags.confirmedPassword = true;
        curState.errorMessages.push('Contraseñas no coinciden');
      });
    }
  };

  handleFieldChange = (fieldName, newValue) => {
    this.setState({ fields: { ...this.state.fields, [fieldName]: newValue } });
  };

  render() {
    const { fields, loading, success, errorMessages, errorFlags } = this.state;

    return (
      <div>
        <div className="signup-form">
          <Grid textAlign="center" verticalAlign="middle">
            <Grid.Column style={{ maxWidth: 600 }}>
              <Message
                attached
                header="Registro"
                content="Uso exlusivo para integrantes del Grupo Sirius"
              />
              <Form
                size="large"
                onSubmit={this.handleSubmit}
                className="attached fluid segment"
                loading={loading}
                success={success}
                error={errorMessages.length > 0}
              >
                <Image size="medium" src={logo} centered />
                <Divider hidden />
                <Social />
                <Divider section />
                <Message
                  error
                  header="Hay problemas con los datos ingresados:"
                  list={errorMessages}
                />
                <Message
                  success
                  header="Solicitud completada"
                  content="Ahora debes esperar que tu cuenta sea aprobada por los administradores."
                />
                <Form.Input
                  fluid
                  icon="add user"
                  iconPosition="left"
                  placeholder="Usuario"
                  value={fields.username}
                  error={errorFlags.username}
                  required
                  onChange={(e, { value }) =>
                    this.handleFieldChange('username', value)}
                />
                <Form.Input
                  fluid
                  icon="mail"
                  iconPosition="left"
                  placeholder="Correo Electrónico"
                  type="email"
                  value={fields.email}
                  error={errorFlags.email}
                  required
                  onChange={(e, { value }) =>
                    this.handleFieldChange('email', value)}
                />
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    icon="user"
                    iconPosition="left"
                    placeholder="Nombre(s)"
                    type="text"
                    value={fields.firstName}
                    error={errorFlags.firstName}
                    required
                    onChange={(e, { value }) =>
                      this.handleFieldChange('firstName', value)}
                  />
                  <Form.Input
                    fluid
                    icon="user"
                    iconPosition="left"
                    placeholder="Apellido(s)"
                    type="text"
                    value={fields.lastName}
                    error={errorFlags.lastName}
                    required
                    onChange={(e, { value }) =>
                      this.handleFieldChange('lastName', value)}
                  />
                </Form.Group>
                <Form.Group widths="equal">
                  <Form.Input
                    fluid
                    placeholder="Contraseña"
                    type="password"
                    icon="lock"
                    iconPosition="left"
                    value={fields.password}
                    error={errorFlags.password}
                    required
                    onChange={(e, { value }) =>
                      this.handleFieldChange('password', value)}
                  />
                  <Form.Input
                    fluid
                    placeholder="Confirmar Contraseña"
                    type="password"
                    icon="unlock alternate"
                    iconPosition="left"
                    value={fields.confirmedPassword}
                    error={errorFlags.confirmedPassword}
                    required
                    onChange={(e, { value }) =>
                      this.handleFieldChange('confirmedPassword', value)}
                  />
                </Form.Group>
                <Button type="submit">ENVIAR</Button>
              </Form>
              <Message attached="bottom" warning>
                <Icon name="help" />
                ¿Ya tienes una cuenta? <a href="/login">Iniciar Sesión</a>
              </Message>
            </Grid.Column>
          </Grid>
        </div>
      </div>
    );
  }
}

export default SignUp;
