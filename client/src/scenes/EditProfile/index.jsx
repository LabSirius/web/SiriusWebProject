import React from 'react';
import { Grid, Icon, Label, Menu, Tab } from 'semantic-ui-react';
import axios from 'axios';

import Info from './components/Info';
import Education from './components/Education';
import Skill from './components/Skill';
import Paper from './components/Paper';

class EditProfile extends React.Component {
  state = {
    loading: false,
    success: false,
    user: {}
  };

  handleChange = () => {
    const { match: { params: { username } } } = this.props;
    const url = '/api/users/' + username;

    axios
      .get(url)
      .then(res => {
        this.setState({
          success: true,
          user: res.data.data
        });
        // console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });
  };

  componentWillMount = () => {
    const { match: { params: { username } } } = this.props;
    const url = '/api/users/' + username;

    axios
      .get(url)
      .then(res => {
        this.setState({
          success: true,
          user: res.data.data
        });
        // console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });
    this.setState({ loading: false });
  };

  render() {
    const panes = [
      {
        menuItem: { key: 'user', icon: 'user', content: 'Información' },
        render: () => (
          <Tab.Pane>
            <Info user={this.state.user} />
          </Tab.Pane>
        )
      },
      {
        menuItem: {
          key: 'graduation',
          icon: 'graduation',
          content: 'Educación'
        },
        render: () => (
          <Tab.Pane>
            <Education user={this.state.user} />
          </Tab.Pane>
        )
      },
      {
        menuItem: { key: 'send', icon: 'send', content: 'Paper' },
        render: () => (
          <Tab.Pane>
            <Paper user={this.state.user} />
          </Tab.Pane>
        )
      },
      {
        menuItem: { key: 'check', icon: 'check', content: 'Habilidades' },
        render: () => (
          <Tab.Pane>
            <Skill user={this.state.user} />
          </Tab.Pane>
        )
      },
      {
        menuItem: { key: 'tasks', icon: 'tasks', content: 'Proyectos' },
        render: () => (
          <Tab.Pane>
            <Info user={this.state.user} />
          </Tab.Pane>
        )
      }
    ];

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={16}>
            <Tab panes={panes} onTabChange={this.handleChange} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default EditProfile;
