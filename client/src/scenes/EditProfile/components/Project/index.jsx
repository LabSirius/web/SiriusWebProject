import React from 'react';
import { Header, Icon, Divider } from 'semantic-ui-react';

class Project extends React.Component {
  state = {};

  render() {
    return (
      <div>
        <Header as="h3">
          <Icon name="tasks" />
          <Header.Content>Proyectos</Header.Content>
        </Header>
        <Divider hidden />
      </div>
    );
  }
}

export default Project;
