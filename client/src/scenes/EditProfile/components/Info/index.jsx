import React from 'react';
import {
  Icon,
  Header,
  Image,
  Divider,
  List,
  Grid,
  Form,
  Input,
  TextArea,
  Button,
  Message
} from 'semantic-ui-react';
import axios from 'axios';

import links from './data/links.json';

import photo from './images/photo.png';

const siteStyle = {
  textAlign: 'center'
};

class Profile extends React.Component {
  state = {
    success: false,
    fields: {
      _id: '',
      accountStatus: '',
      education: [],
      email: '',
      firstName: '',
      lastName: '',
      papers: [],
      skills: [],
      username: '',
      profession: '',
      phoneNumber: '',
      website: '',
      cv: '',
      picture: '',
      role: '',
      description: '',
      socialNetworks: {
        facebook: '',
        twitter: '',
        linkedin: '',
        github: '',
        youtube: ''
      }
    }
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.user !== this.props.user) {
      this.setState({
        fields: nextProps.user
      });
    }
  };

  handleFieldChange = (fieldName, newValue) => {
    this.setState({
      success: false,
      fields: { ...this.state.fields, [fieldName]: newValue }
    });
  };

  handleFieldSocialChange = (fieldName, newValue) => {
    this.setState({
      success: false,
      fields: {
        ...this.state.fields,
        socialNetworks: {
          ...this.state.fields.socialNetworks,
          [fieldName]: newValue
        }
      }
    });
  };

  handleSubmit = s => {
    const url = '/api/users/' + this.state.fields.username;

    axios
      .put(url, this.state.fields)
      .then(res => {
        this.setState({
          success: true
        });
        console.log(':)');
      })
      .catch(err => console.log(err));
    this.setState({ loading: false });
  };

  render() {
    const { fields, success } = this.state;

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={8} computer={8}>
            <div style={siteStyle}>
              <Image src={photo} size="small" rounded centered />
              <Divider hidden />
              <Form onSubmit={this.handleSubmit} success={success}>
                <Message
                  success
                  header="Perfil Editado"
                  content="Los cambios son ahora visibles"
                />
                <Form.Group stackable widths="equal">
                  <Form.Input
                    fluid
                    placeholder="Nombre(s)"
                    type="text"
                    value={fields.firstName}
                    label="Nombre(s)"
                    required
                    onChange={(e, { value }) =>
                      this.handleFieldChange('firstName', value)}
                  />
                  <Form.Input
                    fluid
                    placeholder="Apellido(s)"
                    type="text"
                    value={fields.lastName}
                    label="Apellido(s)"
                    required
                    onChange={(e, { value }) =>
                      this.handleFieldChange('lastName', value)}
                  />
                </Form.Group>
                <Form.Input
                  fluid
                  placeholder="Profesión"
                  type="text"
                  value={fields.profession}
                  label="Profesión"
                  onChange={(e, { value }) =>
                    this.handleFieldChange('profession', value)}
                />
                <Form.Input
                  fluid
                  placeholder="Teléfono"
                  type="text"
                  value={fields.role}
                  label="Rol en Sirius"
                  onChange={(e, { value }) =>
                    this.handleFieldChange('role', value)}
                />
                <Divider />
                <Form.Input
                  fluid
                  icon="phone"
                  iconPosition="left"
                  placeholder="Teléfono"
                  type="text"
                  value={fields.phoneNumber}
                  onChange={(e, { value }) =>
                    this.handleFieldChange('phoneNumber', value)}
                />
                <Form.Input
                  fluid
                  icon="at"
                  iconPosition="left"
                  placeholder="Correo Electrónico"
                  type="email"
                  value={fields.email}
                  required
                  onChange={(e, { value }) =>
                    this.handleFieldChange('email', value)}
                />
                <Form.Input
                  fluid
                  icon="world"
                  iconPosition="left"
                  placeholder="Sitio Web"
                  type="text"
                  value={fields.website}
                  onChange={(e, { value }) =>
                    this.handleFieldChange('website', value)}
                />
                <Button type="submit">ENVIAR</Button>
              </Form>
            </div>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={8} computer={8}>
            <div style={siteStyle}>
              <Divider hidden />
              <Form onSubmit={this.handleSubmit}>
                <Header as="h4">
                  <Icon name="computer" />
                  <Header.Content>Redes Sociales</Header.Content>
                </Header>
                <Form.Input
                  fluid
                  icon="facebook f"
                  iconPosition="left"
                  placeholder="Facebook"
                  type="text"
                  value={
                    fields.socialNetworks ? fields.socialNetworks.facebook : ''
                  }
                  onChange={(e, { value }) =>
                    this.handleFieldSocialChange('facebook', value)}
                />
                <Form.Input
                  fluid
                  icon="twitter"
                  iconPosition="left"
                  placeholder="Twitter"
                  type="text"
                  value={
                    fields.socialNetworks ? fields.socialNetworks.twitter : ''
                  }
                  onChange={(e, { value }) =>
                    this.handleFieldSocialChange('twitter', value)}
                />
                <Form.Input
                  fluid
                  icon="linkedin"
                  iconPosition="left"
                  placeholder="LinkedIn"
                  type="text"
                  value={
                    fields.socialNetworks ? fields.socialNetworks.linkedin : ''
                  }
                  onChange={(e, { value }) =>
                    this.handleFieldSocialChange('linkedin', value)}
                />
                <Form.Input
                  fluid
                  icon="github alternate"
                  iconPosition="left"
                  placeholder="Github"
                  type="text"
                  value={
                    fields.socialNetworks ? fields.socialNetworks.github : ''
                  }
                  onChange={(e, { value }) =>
                    this.handleFieldSocialChange('github', value)}
                />
                <Form.Input
                  fluid
                  icon="youtube play"
                  iconPosition="left"
                  placeholder="Youtube"
                  type="text"
                  value={
                    fields.socialNetworks ? fields.socialNetworks.youtube : ''
                  }
                  onChange={(e, { value }) =>
                    this.handleFieldSocialChange('youtube', value)}
                />
                <Header as="h4">
                  <Icon name="user" />
                  <Header.Content>Acerca de</Header.Content>
                </Header>
                <Form.TextArea
                  fluid
                  placeholder="Descripción"
                  type="text"
                  value={fields.description}
                  onChange={(e, { value }) =>
                    this.handleFieldChange('description', value)}
                />
                <Button type="submit">Actualizar Perfil</Button>
              </Form>
            </div>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default Profile;
