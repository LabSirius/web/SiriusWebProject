import React from 'react';
import {
  List,
  Header,
  Icon,
  Divider,
  Grid,
  Form,
  Message,
  Button,
  Segment
} from 'semantic-ui-react';

import education from './data/education.json';
import axios from 'axios';

class Education extends React.Component {
  state = {
    success: false,
    fields: {
      _id: '',
      accountStatus: '',
      education: [],
      email: '',
      firstName: '',
      lastName: '',
      papers: [],
      skills: [],
      username: '',
      profession: '',
      phoneNumber: '',
      website: '',
      cv: '',
      picture: '',
      role: '',
      description: '',
      socialNetworks: {
        facebook: '',
        twitter: '',
        linkedin: '',
        github: '',
        youtube: ''
      }
    },
    education: {
      title: '',
      institute: '',
      startYear: 0,
      endYear: 0,
      description: ''
    }
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.user !== this.props.user) {
      this.setState({
        fields: nextProps.user
      });
    }
  };

  handleFieldChange = (fieldName, newValue) => {
    this.setState({
      success: false,
      education: { ...this.state.education, [fieldName]: newValue }
    });
  };

  handleSubmit = s => {
    const url = '/api/users/' + this.state.fields.username;
    const allEducation = this.state.fields.education;
    allEducation.push(this.state.education);

    this.setState({
      fields: {
        ...this.state.fields,
        education: allEducation
      },
      education: {
        title: '',
        institute: '',
        startYear: 0,
        endYear: 0,
        description: ''
      }
    });

    axios
      .put(url, this.state.fields)
      .then(res => {
        this.setState({
          success: true
        });
        console.log(':)');
      })
      .catch(err => console.log(err));
  };

  render() {
    const { fields, success } = this.state;
    const educationList = fields.education;
    const education = this.state.education;

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={16}>
            <Header as="h4">
              <Icon name="plus" />
              <Header.Content>Agregar</Header.Content>
            </Header>
            <Form onSubmit={this.handleSubmit} success={success}>
              <Message
                success
                header="Perfil Editado"
                content="Los cambios son ahora visibles"
              />
              <Form.Group widths="equal">
                <Form.Input
                  fluid
                  placeholder="Título"
                  value={education.title}
                  type="text"
                  label="Título"
                  onChange={(e, { value }) =>
                    this.handleFieldChange('title', value)}
                />
                <Form.Input
                  fluid
                  placeholder="Institución"
                  value={education.institute}
                  type="text"
                  label="Institución"
                  onChange={(e, { value }) =>
                    this.handleFieldChange('institute', value)}
                />
              </Form.Group>
              <Form.Group stacable widths="equal">
                <Form.Input
                  fluid
                  placeholder="Fecha de Inicio"
                  value={education.startYear}
                  type="number"
                  label="Fecha de Inicio"
                  onChange={(e, { value }) =>
                    this.handleFieldChange('startYear', value)}
                />
                <Form.Input
                  fluid
                  placeholder="Fecha de Finalización"
                  value={education.endYear}
                  type="number"
                  label="Fecha de Finalización"
                  onChange={(e, { value }) =>
                    this.handleFieldChange('endYear', value)}
                />
              </Form.Group>
              <Form.TextArea
                fluid
                placeholder="Descripción"
                value={education.description}
                type="text"
                onChange={(e, { value }) =>
                  this.handleFieldChange('description', value)}
              />
              <Button type="submit">Actualizar Perfil</Button>
            </Form>
            <Divider />
            <Header as="h4">
              <Icon name="edit" />
              <Header.Content>Editar</Header.Content>
            </Header>
            {educationList.length > 0
              ? educationList.map(
                  (
                    {
                      title,
                      institute,
                      startYear,
                      endYear,
                      description
                    }: linkType,
                    i: number
                  ) =>
                    <Segment key={i}>
                      <Grid>
                        <Grid.Row>
                          <Grid.Column mobile={16} tablet={3} computer={3}>
                            <b>
                              {startYear} - {endYear}
                            </b>
                          </Grid.Column>
                          <Grid.Column mobile={16} tablet={13} computer={13}>
                            <List>
                              <List.Item as="b">
                                {title}
                              </List.Item>
                              <List.Item as="i">
                                {institute}
                              </List.Item>
                              <List.Item>
                                {description}
                              </List.Item>
                            </List>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Segment>
                )
              : 'No hay education'}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default Education;
