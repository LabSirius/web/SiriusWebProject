import React from 'react';
import {
  Progress,
  Header,
  Icon,
  Divider,
  Grid,
  Form,
  Message,
  Button
} from 'semantic-ui-react';

// import skills from './data/skill.json';
import axios from 'axios';

class Skill extends React.Component {
  state = {
    success: false,
    fields: {
      _id: '',
      accountStatus: '',
      education: [],
      email: '',
      firstName: '',
      lastName: '',
      papers: [],
      skills: [],
      username: '',
      profession: '',
      phoneNumber: '',
      website: '',
      cv: '',
      picture: '',
      role: '',
      description: '',
      socialNetworks: {
        facebook: '',
        twitter: '',
        linkedin: '',
        github: '',
        youtube: ''
      }
    },
    skills: {
      name: '',
      level: 0
    }
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.user !== this.props.user) {
      this.setState({
        fields: nextProps.user
      });
    }
  };

  handleFieldChange = (fieldName, newValue) => {
    this.setState({
      success: false,
      skills: { ...this.state.skills, [fieldName]: newValue }
    });
  };

  handleSubmit = s => {
    const url = '/api/users/' + this.state.fields.username;
    const allSkills = this.state.fields.skills;
    allSkills.push(this.state.skills);

    this.setState({
      fields: {
        ...this.state.fields,
        skills: allSkills
      },
      skills: {
        name: '',
        level: 0
      }
    });

    axios
      .put(url, this.state.fields)
      .then(res => {
        this.setState({
          success: true
        });
        console.log(':)');
      })
      .catch(err => console.log(err));
  };

  render() {
    const { fields, success } = this.state;
    const skillsList = fields.skills;
    const skills = this.state.skills;

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={8} computer={4}>
            {skillsList.length > 0
              ? skillsList.map(({ name, level }: linkType, i: number) =>
                  <Progress key={i} percent={level} label={name} progress />
                )
              : null}
          </Grid.Column>
        </Grid.Row>
        <Divider />
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={16}>
            <Form onSubmit={this.handleSubmit} success={success}>
              <Message
                success
                header="Perfil Editado"
                content="Los cambios son ahora visibles"
              />
              <Form.Group stackable widths="equal">
                <Form.Input
                  fluid
                  placeholder="Nombre Skill"
                  type="text"
                  value={skills.name}
                  label="Nombre Skills"
                  required
                  onChange={(e, { value }) =>
                    this.handleFieldChange('name', value)}
                />
                <Form.Input
                  fluid
                  placeholder="Nivel de Skill"
                  type="number"
                  value={skills.level}
                  label="Nivel de Skill"
                  required
                  onChange={(e, { value }) =>
                    this.handleFieldChange('level', value)}
                />
              </Form.Group>
              <Button type="submit">Actualizar Perfil</Button>
            </Form>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default Skill;
