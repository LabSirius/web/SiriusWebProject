import React from 'react';
import {
  Header,
  Icon,
  Grid,
  Divider,
  List,
  Form,
  Message,
  Button,
  Segment
} from 'semantic-ui-react';

import axios from 'axios';

// import paper from './data/paper.json';

class Paper extends React.Component {
  state = {
    success: false,
    fields: {
      _id: '',
      accountStatus: '',
      education: [],
      email: '',
      firstName: '',
      lastName: '',
      papers: [],
      skills: [],
      username: '',
      profession: '',
      phoneNumber: '',
      website: '',
      cv: '',
      picture: '',
      role: '',
      description: '',
      socialNetworks: {
        facebook: '',
        twitter: '',
        linkedin: '',
        github: '',
        youtube: ''
      }
    },
    papers: {
      date: '',
      title: '',
      publisher: '',
      url: ''
    }
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.user !== this.props.user) {
      this.setState({
        fields: nextProps.user
      });
    }
  };

  handleFieldChange = (fieldName, newValue) => {
    this.setState({
      success: false,
      papers: { ...this.state.papers, [fieldName]: newValue }
    });
  };

  handleSubmit = s => {
    const url = '/api/users/' + this.state.fields.username;
    const allPapers = this.state.fields.papers;
    allPapers.push(this.state.papers);

    this.setState({
      fields: {
        ...this.state.fields,
        papers: allPapers
      },
      papers: {
        date: '',
        publisher: '',
        url: ''
      }
    });

    axios
      .put(url, this.state.fields)
      .then(res => {
        this.setState({
          success: true
        });
        console.log(':)');
      })
      .catch(err => console.log(err));
  };

  render() {
    const { fields, success } = this.state;
    const papersList = fields.papers;
    const papers = this.state.papers;

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={16}>
            <Header as="h4">
              <Icon name="plus" />
              <Header.Content>Agregar</Header.Content>
            </Header>
            <Form onSubmit={this.handleSubmit} success={success}>
              <Message
                success
                header="Perfil Editado"
                content="Los cambios son ahora visibles"
              />
              <Form.Group widths="equal">
                <Form.Input
                  fluid
                  placeholder="Fecha"
                  value={papers.date}
                  type="date"
                  label="Fecha"
                  onChange={(e, { value }) =>
                    this.handleFieldChange('date', value)
                  }
                />
                <Form.Input
                  fluid
                  placeholder="Título"
                  value={papers.title}
                  type="text"
                  label="Título"
                  onChange={(e, { value }) =>
                    this.handleFieldChange('title', value)
                  }
                />
              </Form.Group>
              <Form.Group widths="equal">
                <Form.Input
                  fluid
                  placeholder="Revista"
                  value={papers.publisher}
                  type="text"
                  label="Revista"
                  onChange={(e, { value }) =>
                    this.handleFieldChange('publisher', value)
                  }
                />
                <Form.Input
                  fluid
                  placeholder="URL / Link / Referencia"
                  value={papers.url}
                  type="text"
                  label="URL / Link / Referencia"
                  onChange={(e, { value }) =>
                    this.handleFieldChange('url', value)
                  }
                />
              </Form.Group>
              <Form.TextArea
                fluid
                placeholder="Descripción"
                value={papers.description}
                type="text"
                onChange={(e, { value }) =>
                  this.handleFieldChange('description', value)
                }
              />
              <Button type="submit">Actualizar Perfil</Button>
            </Form>
            <Divider />
            <Header as="h4">
              <Icon name="edit" />
              <Header.Content>Editar</Header.Content>
            </Header>
            {papersList.length > 0
              ? papersList.map(
                  (
                    { date, title, publisher, description, url }: linkType,
                    i: number
                  ) => (
                    <Segment key={i}>
                      <Grid>
                        <Grid.Row>
                          <Grid.Column
                            mobile={16}
                            tablet={3}
                            computer={3}
                            as="b"
                          >
                            {new Date(date).toISOString().slice(0, 10)}
                          </Grid.Column>
                          <Grid.Column mobile={16} tablet={13} computer={13}>
                            <List>
                              <List.Item as="b">{title}</List.Item>
                              <List.Item as="i">{publisher}</List.Item>
                              <List.Item>{description}</List.Item>
                              <List.Item as="i">{url}</List.Item>
                            </List>
                          </Grid.Column>
                        </Grid.Row>
                      </Grid>
                    </Segment>
                  )
                )
              : null}
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default Paper;
