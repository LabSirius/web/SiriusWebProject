import React from 'react';
import { Grid, Segment, Divider, Search, Pagination } from 'semantic-ui-react';
import axios from 'axios';

import LastUsers from './components/LastUsers';
import ListUsers from './components/ListUsers';

class Users extends React.Component {
  state = {
    loading: false,
    success: false,
    users: []
  };

  componentWillMount() {
    this.setState({ loading: true });

    axios
      .get('/api/users')
      .then(res => {
        this.setState({
          success: true,
          users: res.data.data
        });
        // console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });
    this.setState({ loading: false });
  }

  render() {
    return (
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={16} tablet={6} computer={4}>
              <Segment>
                <Search
                  input={{ icon: 'search', iconPosition: 'left' }}
                  fluid
                />
              </Segment>
              <LastUsers usersList={this.state.users} />
              <Divider hidden />
            </Grid.Column>
            <Grid.Column mobile={16} tablet={10} computer={12}>
              <ListUsers usersList={this.state.users} />
              <Divider hidden />
              <Pagination size="mini" defaultActivePage={3} totalPages={5} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default Users;
