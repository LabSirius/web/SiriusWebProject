import React from 'react';
import { Card, Feed } from 'semantic-ui-react';

import photo from './images/photo.jpg';
// import list from './data/list.json';

type listType = { firstName: string, lastName: string, username: string };

class LastUsers extends React.Component {
  state = {};

  render() {
    const usersList = this.props.usersList;

    return (
      <Card>
        {console.log('usersList', usersList)}
        <Card.Content>
          <Card.Header>Últimos Integrantes</Card.Header>
        </Card.Content>
        <Card.Content>
          {usersList.map(
            ({ firstName, lastName, username }: listType, i: number) =>
              <Feed key={i}>
                <Feed.Event>
                  <Feed.Label image={photo} />
                  <Feed.Content>
                    {/* <Feed.Date content={date} /> */}
                    <Feed.Summary>
                      <a href={'/users/' + username}>
                        {firstName + ' ' + lastName}
                      </a>
                    </Feed.Summary>
                    <Feed.Summary>
                      {lastName}
                    </Feed.Summary>
                  </Feed.Content>
                </Feed.Event>
              </Feed>
          )}
        </Card.Content>
      </Card>
    );
  }
}

export default LastUsers;
