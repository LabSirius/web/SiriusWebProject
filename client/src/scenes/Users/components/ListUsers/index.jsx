import React from 'react';
import {
  Divider,
  Card,
  Image,
  Icon,
  Button,
  Modal,
  Header
} from 'semantic-ui-react';

import photo from './images/photo.jpg';
import logo from './images/sirius.svg';
import list from './data/list.json';

type listType = { name: string, date: string, profession: string };

class ListUsers extends React.Component {
  render() {
    return (
      <div>
        <Header as="h2" icon textAlign="center">
          <Icon name="users" circular />
          <Header.Content>INTEGRANTES</Header.Content>
        </Header>
        <Image centered size="medium" src={logo} />
        <Divider hidden />
        <Card.Group itemsPerRow={3} stackable>
          {list.map(({ name, date, profession }: listType, i: number) =>
            <Card key={i}>
              <Image src={photo} rounded spaced centered />
              <Card.Content>
                <Card.Header>
                  {name}
                </Card.Header>
                <Card.Meta>
                  {profession}
                </Card.Meta>
              </Card.Content>
              <Card.Content extra>
                <div className="ui two buttons">
                  <Button icon="user" color="green" />
                  {/* <Modal
                    trigger={<Button icon="search" />}
                    actions={[
                      'Snooze',
                      { key: 'done', content: 'Done', positive: true }
                    ]}
                  >
                    <Modal.Header>
                      {name}
                    </Modal.Header>
                    <Modal.Content image>
                      <Image wrapped size="medium" src={photo} />
                      <Modal.Description>
                        <Header>Default Profile Image</Header>
                        <p>
                          We've found the following gravatar image associated
                          with your e-mail address.
                        </p>
                        <p>Is it okay to use this photo?</p>
                      </Modal.Description>
                    </Modal.Content>
                    <Modal.Actions>
                      <Button color="black" onClick={this.close}>
                        Nope
                      </Button>
                      <Button
                        positive
                        icon="checkmark"
                        labelPosition="right"
                        content="Yep, that's me"
                        onClick={this.close}
                      />
                    </Modal.Actions>
                  </Modal> */}
                </div>
              </Card.Content>
            </Card>
          )}
        </Card.Group>
      </div>
    );
  }
}

export default ListUsers;
