import React from 'react';
import { Icon, Header } from 'semantic-ui-react';

import links from './data/links.json';

const siteStyle = {
  textAlign: 'center'
};

class Social extends React.Component {
  state = {};

  render() {
    return (
      <div style={siteStyle}>
        <Header as="h4">
          <Icon name="comments" />
          <Header.Content>Redes Sociales</Header.Content>
        </Header>
        {links.map(({ icon, href, color }: linkType, i: number) =>
          <a key={i} target="_blank" href={href}>
            <Icon
              link
              bordered
              inverted
              size="large"
              name={icon}
              color={color}
            />
          </a>
        )}
      </div>
    );
  }
}

export default Social;
