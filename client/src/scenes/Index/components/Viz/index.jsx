import React from 'react';
import { List, Icon, Header } from 'semantic-ui-react';

import viz from './data/list.json';

class Viz extends React.Component {
  state = {};

  render() {
    return (
      <div>
        <Header as="h4">
          <Icon name="folder" />
          <Header.Content>Visualizaciones</Header.Content>
        </Header>
        <List>
          <List.Item>
            <List.Icon name='folder' />
            <List.Content>
              <List.Header>Algunas Visualizaciones</List.Header>
              <List.Description>Ejemplo de Visualizaciones</List.Description>
              <List.List>
              {viz.map(({ title, href, description }: linkType, i: number) =>
                <List.Item key={i}>
                  <List.Icon name='file' />
                  <List.Content>
                    <List.Header>
                      <a href="{href}">{title}</a>
                    </List.Header>
                    <List.Description>{description}</List.Description>
                  </List.Content>
                </List.Item>
              )}
              </List.List>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
  }
}

export default Viz;