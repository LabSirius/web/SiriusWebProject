import React from 'react';
import { Grid, Segment } from 'semantic-ui-react';

import Social from './components/Social';
import Viz from './components/Viz';
import News from './components/News';

class Index extends React.Component {
  state = {};

  render() {
    return (
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={16} tablet={6} computer={6}>
              <Segment>
                <Social />
              </Segment>
              <Segment>
                <Viz />
              </Segment>
            </Grid.Column>
            <Grid.Column mobile={16} tablet={10} computer={10}>
              <News />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default Index;
