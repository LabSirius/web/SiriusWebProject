import React from 'react';
import { Grid } from 'semantic-ui-react';

import Main from './components/Main';
import News from './components/News';

class New extends React.Component {
  state = {};

  render() {
    const { match: { params: { newId } } } = this.props;

    return (
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={16} tablet={10} computer={12}>
              <h1>{`New id: ${newId}`}</h1>
              <Main />
            </Grid.Column>
            <Grid.Column mobile={16} tablet={6} computer={4}>
              <News />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default New;
