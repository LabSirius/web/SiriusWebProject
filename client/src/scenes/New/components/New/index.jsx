import React from 'react';
import { Grid, Image, Divider, Header, Feed } from 'semantic-ui-react';

import profile from './images/photo.png';

class New extends React.Component {
  state = {};

  render() {
    return (
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={16} tablet={16} computer={16}>
              <Header as="h3">
                <Header.Content>
                  {this.props.title}
                </Header.Content>
              </Header>
              <p>
                {this.props.content}
              </p>
              <Image src={profile} />
              <Divider />
              <Feed>
                <Feed.Event>
                  <Feed.Label image={profile} />
                  <Feed.Content>
                    <Feed.Date content="1 day ago" />
                    <Feed.Summary>
                      Publicado por <a>{this.props.username}</a>.
                    </Feed.Summary>
                  </Feed.Content>
                </Feed.Event>
              </Feed>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default New;
