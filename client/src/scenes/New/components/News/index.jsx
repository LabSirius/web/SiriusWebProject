import React from 'react';
import { Icon, Segment, Header } from 'semantic-ui-react';

import New from '../New';
import news from './data/news.json';

class News extends React.Component {
  state = {};

  render() {
    return (
      <div>
        <Header as="h4">
          <Icon name="newspaper" />
          <Header.Content>Últimas Noticias</Header.Content>
        </Header>
        {news.map(
          (
            { title, username, usernameImage, content, image }: linkType,
            i: number
          ) =>
            <Segment>
              <New
                key={i}
                title={title}
                username={username}
                usernameImage={usernameImage}
                content={content}
                image={image}
              />
            </Segment>
        )}
      </div>
    );
  }
}

export default News;
