import React from 'react';
import { Grid, Icon, Divider } from 'semantic-ui-react';
import axios from 'axios';

import Info from './components/Info';
import Skill from './components/Skill';
import Education from './components/Education';
import Paper from './components/Paper';
import Project from './components/Project';

class Profile extends React.Component {
  state = {
    loading: false,
    success: false,
    user: {}
  };

  componentWillMount() {
    const { match: { params: { username } } } = this.props;
    const url = '/api/users/' + username;

    axios
      .get(url)
      .then(res => {
        this.setState({
          success: true,
          user: res.data.data
        });
        // console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });
    this.setState({ loading: false });
  }

  render() {
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={16} textAlign="right">
            <a target="_blank" href="">
              <Icon link size="large" name="file pdf outline" color="red" />
            </a>
          </Grid.Column>
        </Grid.Row>
        <Divider hidden />
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={5}>
            <Info user={this.state.user} />
          </Grid.Column>
          <Grid.Column mobile={16} tablet={16} computer={11}>
            <Education user={this.state.user} />
            <Divider section />
            <Paper user={this.state.user} />
          </Grid.Column>
        </Grid.Row>
        <Divider section />
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={4}>
            <Skill user={this.state.user} />
          </Grid.Column>
        </Grid.Row>
        <Divider section />
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={4}>
            <Project />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default Profile;
