import React from 'react';
import { Progress, Header, Icon, Divider } from 'semantic-ui-react';

import skills from './data/skill.json';

class Skill extends React.Component {
  state = {
    fields: {
      _id: '',
      accountStatus: '',
      education: [],
      email: '',
      firstName: '',
      lastName: '',
      papers: [],
      skills: [],
      username: '',
      profession: '',
      phoneNumber: '',
      website: '',
      cv: '',
      picture: '',
      role: '',
      description: '',
      socialNetworks: {
        facebook: '',
        twitter: '',
        linkedin: '',
        github: '',
        youtube: ''
      }
    }
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.user !== this.props.user) {
      this.setState({
        fields: nextProps.user
      });
    }
  };

  render() {
    const { fields } = this.state;
    const skills = fields.skills;

    console.log(fields);

    return (
      <div>
        <Header as="h3">
          <Icon name="check" />
          <Header.Content>Habilidades</Header.Content>
        </Header>
        <Divider hidden />
        {skills.map(({ name, level }: linkType, i: number) =>
          <Progress key={i} percent={level} label={name} progress />
        )}
      </div>
    );
  }
}

export default Skill;
