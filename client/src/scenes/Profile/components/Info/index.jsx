import React from 'react';
import { Icon, Header, Image, Divider, List } from 'semantic-ui-react';

import links from './data/links.json';

import photo from './images/photo.png';

const siteStyle = {
  textAlign: 'center'
};

class Profile extends React.Component {
  state = {
    fields: {
      _id: '',
      accountStatus: '',
      education: [],
      email: '',
      firstName: '',
      lastName: '',
      papers: [],
      skills: [],
      username: '',
      profession: '',
      phoneNumber: '',
      website: '',
      cv: '',
      picture: '',
      role: '',
      description: '',
      socialNetworks: {
        facebook: '',
        twitter: '',
        linkedin: '',
        github: '',
        youtube: ''
      }
    }
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.user !== this.props.user) {
      this.setState({
        fields: nextProps.user
      });
    }
  };

  render() {
    const user = this.state.fields;

    return (
      <div style={siteStyle}>
        <Image src={photo} size="small" rounded centered />
        <Header as="h4">
          {user.firstName + ' ' + user.lastName}
        </Header>
        <p>
          {user.profession ? user.profession : null}
          <br />
          {user.role ? user.role : null}
        </p>
        {user.socialNetworks && user.socialNetworks.facebook
          ? <a
              target="_blank"
              href={'http://facebook.com/' + user.socialNetworks.facebook}
            >
              <Icon
                link
                circular
                inverted
                size="large"
                name="facebook f"
                color="blue"
              />
            </a>
          : null}
        {user.socialNetworks && user.socialNetworks.twitter
          ? <a
              target="_blank"
              href={'http://twitter.com/' + user.socialNetworks.twitter}
            >
              <Icon
                link
                circular
                inverted
                size="large"
                name="twitter"
                color="teal"
              />
            </a>
          : null}
        {user.socialNetworks && user.socialNetworks.linkedin
          ? <a
              target="_blank"
              href={'http://linkedin.com/' + user.socialNetworks.linkedin}
            >
              <Icon
                link
                circular
                inverted
                size="large"
                name="linkedin"
                color="blue"
              />
            </a>
          : null}
        {user.socialNetworks && user.socialNetworks.github
          ? <a
              target="_blank"
              href={'http://github.com/' + user.socialNetworks.github}
            >
              <Icon
                link
                circular
                inverted
                size="large"
                name="github alternate"
                color="black"
              />
            </a>
          : null}
        {user.socialNetworks && user.socialNetworks.youtube
          ? <a
              target="_blank"
              href={'http://youtube.com/' + user.socialNetworks.youtube}
            >
              <Icon
                link
                circular
                inverted
                size="large"
                name="youtube play"
                color="red"
              />
            </a>
          : null}
        <List>
          {user.phoneNumber
            ? <List.Item>
                <List.Icon name="phone" />
                <List.Content>
                  {user.phoneNumber}
                </List.Content>
              </List.Item>
            : null}
          <List.Item>
            <List.Icon name="at" />
            <List.Content>
              <a href={'mailto:' + user.email}>
                {user.email}
              </a>
            </List.Content>
          </List.Item>
          {user.website
            ? <List.Item>
                <List.Icon name="world" />
                <List.Content>
                  <a href={user.website}>
                    {user.website}
                  </a>
                </List.Content>
              </List.Item>
            : null}
        </List>
        <Divider />
        <Header as="h4">
          <Icon name="user" />
          <Header.Content>Acerca de</Header.Content>
        </Header>
        {user.description ? user.description : 'No hay descripción'}
        <Divider hidden />
      </div>
    );
  }
}

export default Profile;
