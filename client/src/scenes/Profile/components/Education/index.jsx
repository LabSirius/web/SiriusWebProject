import React from 'react';
import { List, Header, Icon, Divider, Grid } from 'semantic-ui-react';

// import education from './data/education.json';

class Education extends React.Component {
  state = {
    fields: {
      _id: '',
      accountStatus: '',
      education: [],
      email: '',
      firstName: '',
      lastName: '',
      papers: [],
      skills: [],
      username: '',
      profession: '',
      phoneNumber: '',
      website: '',
      cv: '',
      picture: '',
      role: '',
      description: '',
      socialNetworks: {
        facebook: '',
        twitter: '',
        linkedin: '',
        github: '',
        youtube: ''
      }
    }
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.user !== this.props.user) {
      this.setState({
        fields: nextProps.user
      });
    }
  };

  render() {
    const { fields } = this.state;
    const education = fields.education;

    return (
      <div>
        <Header as="h3">
          <Icon name="graduation" />
          <Header.Content>Educación</Header.Content>
        </Header>
        <Divider hidden />
        <Grid centered>
          {education.length === 0 ? 'No hay datos' : null}
          {education.map(
            (
              { title, institute, startYear, endYear, description }: linkType,
              i: number
            ) =>
              <Grid.Row key={i}>
                <Grid.Column mobile={16} tablet={16} computer={3}>
                  <b>
                    {startYear} - {endYear}
                  </b>
                </Grid.Column>
                <Grid.Column mobile={16} tablet={16} computer={13}>
                  <List>
                    <List.Item as="b">
                      {title}
                    </List.Item>
                    <List.Item as="i">
                      {institute}
                    </List.Item>
                    <List.Item>
                      {description}
                    </List.Item>
                  </List>
                </Grid.Column>
              </Grid.Row>
          )}
        </Grid>
      </div>
    );
  }
}

export default Education;
