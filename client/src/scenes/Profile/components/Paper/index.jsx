import React from 'react';
import { Header, Icon, Grid, Divider, List } from 'semantic-ui-react';

class Paper extends React.Component {
  state = {
    fields: {
      _id: '',
      accountStatus: '',
      education: [],
      email: '',
      firstName: '',
      lastName: '',
      papers: [],
      skills: [],
      username: '',
      profession: '',
      phoneNumber: '',
      website: '',
      cv: '',
      picture: '',
      role: '',
      description: '',
      socialNetworks: {
        facebook: '',
        twitter: '',
        linkedin: '',
        github: '',
        youtube: ''
      }
    }
  };

  componentWillReceiveProps = nextProps => {
    if (nextProps.user !== this.props.user) {
      this.setState({
        fields: nextProps.user
      });
    }
  };

  render() {
    const { fields } = this.state;
    const papers = fields.papers;

    console.log(papers);

    return (
      <div>
        <Header as="h3">
          <Icon name="send" />
          <Header.Content>Papers</Header.Content>
        </Header>
        <Divider hidden />
        <Grid centered>
          {papers.length === 0 ? 'No hay datos' : null}
          {papers.map(
            (
              { date, title, publisher, description, url }: linkType,
              i: number
            ) => (
              <Grid.Row key={i}>
                <Grid.Column mobile={16} tablet={16} computer={3} as="b">
                  {new Date(date).toISOString().slice(0, 10)}
                </Grid.Column>
                <Grid.Column mobile={16} tablet={16} computer={13}>
                  <List>
                    <List.Item as="b">{title}</List.Item>
                    <List.Item as="i">{publisher}</List.Item>
                    <List.Item>{description}</List.Item>
                    <List.Item as="i">{url}</List.Item>
                  </List>
                </Grid.Column>
              </Grid.Row>
            )
          )}
        </Grid>
      </div>
    );
  }
}

export default Paper;
