import React from 'react';
import { Grid, Image, Divider, Header, Feed, Icon } from 'semantic-ui-react';

import profile from './images/photo.png';

class Main extends React.Component {
  state = {};

  render() {
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column mobile={16} tablet={16} computer={16}>
            <Header as="h1" icon textAlign="center">
              <Icon name="tasks" circular />
              <Header.Content>
                Aquí va el título de los Proyectos
              </Header.Content>
            </Header>
            <Feed>
              <Feed.Event>
                <Feed.Label image={profile} />
                <Feed.Content>
                  <Feed.Date content="1 day ago" />
                  <Feed.Summary>
                    Publicado por <a>Autor</a>.
                  </Feed.Summary>
                </Feed.Content>
              </Feed.Event>
            </Feed>
            <Divider />
            <Image src={profile} centered />
            <Divider />
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
              vehicula arcu sit amet sapien euismod auctor. Aliquam eu sem eu
              diam rhoncus vehicula a ac mauris. Curabitur fermentum mauris mi,
              a auctor tellus dapibus eu. Ut ultrices magna ligula, ut
              sollicitudin erat mollis in. Pellentesque semper vulputate dolor
              consectetur iaculis.
            </p>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
              vehicula arcu sit amet sapien euismod auctor. Aliquam eu sem eu
              diam rhoncus vehicula a ac mauris. Curabitur fermentum mauris mi,
              a auctor tellus dapibus eu. Ut ultrices magna ligula, ut
              sollicitudin erat mollis in. Pellentesque semper vulputate dolor
              consectetur iaculis.
            </p>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
              vehicula arcu sit amet sapien euismod auctor. Aliquam eu sem eu
              diam rhoncus vehicula a ac mauris. Curabitur fermentum mauris mi,
              a auctor tellus dapibus eu. Ut ultrices magna ligula, ut
              sollicitudin erat mollis in. Pellentesque semper vulputate dolor
              consectetur iaculis.
            </p>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis
              vehicula arcu sit amet sapien euismod auctor. Aliquam eu sem eu
              diam rhoncus vehicula a ac mauris. Curabitur fermentum mauris mi,
              a auctor tellus dapibus eu. Ut ultrices magna ligula, ut
              sollicitudin erat mollis in. Pellentesque semper vulputate dolor
              consectetur iaculis.
            </p>
            <Divider />
            <Feed>
              <Feed.Event>
                <Feed.Label image={profile} />
                <Feed.Content>
                  <Feed.Date content="1 day ago" />
                  <Feed.Summary>
                    Publicado por <a>Autor</a>.
                  </Feed.Summary>
                </Feed.Content>
              </Feed.Event>
            </Feed>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default Main;
