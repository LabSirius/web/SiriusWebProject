import React from 'react';
import { Icon, Segment, Header } from 'semantic-ui-react';

import ProjectComponent from '../Project';
import news from './data/news.json';

class Project extends React.Component {
  state = {};

  render() {
    return (
      <div>
        <Header as="h4">
          <Icon name="tasks" />
          <Header.Content>Otros Proyectos</Header.Content>
        </Header>
        {news.map(
          (
            { title, username, usernameImage, content, image }: linkType,
            i: number
          ) =>
            <Segment>
              <ProjectComponent
                key={i}
                title={title}
                username={username}
                usernameImage={usernameImage}
                content={content}
                image={image}
              />
            </Segment>
        )}
      </div>
    );
  }
}

export default Project;
