import React from 'react';
import { Grid } from 'semantic-ui-react';

import Main from './components/Main';
import Projects from './components/Projects';

class Project extends React.Component {
  state = {};

  render() {
    const { match: { params: { projectID } } } = this.props;

    return (
      <div>
        <Grid>
          <Grid.Row>
            <Grid.Column mobile={16} tablet={10} computer={12}>
              <h1>{`New id: ${projectID}`}</h1>
              <Main />
            </Grid.Column>
            <Grid.Column mobile={16} tablet={6} computer={4}>
              <Projects />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default Project;
