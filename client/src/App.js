// @flow
import React from 'react';
import { Container, Divider, Image } from 'semantic-ui-react';
import { Switch, Route } from 'react-router-dom';

import Header from './components/Header';
import NavBar from './components/NavBar';
import Footer from './components/Footer';

import New from './scenes/New';
import Index from './scenes/Index';
import SignUp from './scenes/SignUp';
import Login from './scenes/Login';
import Profile from './scenes/Profile';
import Users from './scenes/Users';
import Project from './scenes/Project';
import EditProfile from './scenes/EditProfile';

import logo from './images/logo.svg';

const siteStyle = {
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column'
};

const contentStyle = {
  flex: 1,
  padding: '100px 0'
};

const copyRight = {
  textAlign: 'center',
  color: '#ccc',
  fontStyle: 'italic',
  fontWeight: 'bold'
};

class App extends React.Component {
  render() {
    return (
      <div style={siteStyle}>
        <NavBar />
        <Header />
        <div style={contentStyle}>
          <Container>
            <Divider section />
            <Switch>
              <Route exact path="/" component={Index} />
              <Route path="/news/:newId" component={New} />
              <Route path="/join" component={SignUp} />
              <Route path="/login" component={Login} />
              <Route exact path="/users" component={Users} />
              <Route exact path="/users/:username" component={Profile} />
              <Route path="/users/:username/edit" component={EditProfile} />
              <Route path="/projects/:projectID" component={Project} />
            </Switch>
            <Divider section />
            <Image src={logo} size="medium" centered />
            <p style={copyRight}>
              © Grupo Sirius. {new Date().getFullYear()}
            </p>
          </Container>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
