let request = require('supertest-as-promised');
const api = require('../app');
const host = api.server; // podemos pasarlo otra url

request = request(host);

describe('Index route, Hello World', () => {
  describe('GET /api', () => {
    it('should return hello world', done => {
      request
        .get('/api')
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          const { body } = res;
          expect(body).to.have.property('message', 'Hello World');
          done();
        }, done);
    });
  });
});
