const mongoose = require('mongoose');
const config = require('../config/test');
const news = require('../models/news');
const faker = require('faker');

describe('Projects route', () => {
  before(() => {
    mongoose.Promise = global.Promise;
    mongoose.connect(config.db.url);
  });

  after(done => {
    mongoose.disconnect(done);
    mongoose.models = {};
  });

  const newData = {
    title: faker.lorem.words(),
    authors: [faker.name.findName(), faker.name.findName()],
    body: faker.lorem.paragraphs(),
    tags: [faker.lorem.word()],
    image: faker.image.imageUrl()
  };

  it('should save a new', done => {
    console.log(newData);
    done();
  });
});
