const app = require('../app');
const supertest = require('supertest-as-promised');
const mongoose = require('mongoose');
const config = require('../config/test');

const api = supertest(app.server); // app.server has the host direction

describe('Projects route', () => {
  before(() => {
    mongoose.Promise = global.Promise;
    mongoose.connect(config.db.url);
  });

  after(done => {
    mongoose.disconnect(done);
    mongoose.models = {};
  });

  describe('GET /api/projects', () => {
    it('should return ok', done => {
      api
        .get('/api/projects')
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          const { body } = res;
          expect(body).to.have.property('data');
          done();
        }, done);
    });
  });
});
