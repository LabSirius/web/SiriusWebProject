let request = require('supertest-as-promised');
const api = require('../app');
const faker = require('faker');
const host = api.server; // podemos pasarlo otra url
const mongoose = require('mongoose');
const config = require('../config/test');
const _ = require('lodash');
request = request(host);

describe('Users route', () => {
  before(() => {
    mongoose.Promise = global.Promise;
    mongoose.connect(config.db.url);
  });

  after(done => {
    mongoose.disconnect(done);
    mongoose.models = {};
  });

  describe('GET /api/users', () => {
    it('should return an array with all users registered', done => {
      let user1Id;
      let user2Id;
      const user1 = {
        firstName: 'Alejandro Esteban',
        lastName: 'Rendon Diosa',
        username: 'aerendon',
        email: 'alejorendon@utp.edu.co',
        password: 'rendon123',
        photo: '/',
        professionalProfile: ['Estudiante'],
        status: ['Monitor']
      };

      const user2 = {
        firstName: 'Leiver Andres',
        lastName: 'Campeon Benjumea',
        username: 'leiver',
        email: 'leiverandres@utp.edu.co',
        password: 'leiver123',
        photo: '/',
        professionalProfile: ['Estudiante'],
        status: ['Monitor']
      };

      request
        .post('/api/users')
        .send(user1)
        .set('Accept', 'application/json')
        .expect(201)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          user1Id = res.body.data._id;
          return request
            .post('/api/users')
            .send(user2)
            .set('Accept', 'application/json')
            .expect(201)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          user2Id = res.body.data._id;
          return request
            .get('/api/users')
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          const { body } = res;
          expect(body).to.have.property('success', true);
          expect(body).to.have.property('data');
          expect(body.data).to.be.an('array').and.to.have.length.above(2);

          const { data } = body;
          const user1 = _.find(data, { _id: user1Id });
          const user2 = _.find(data, { _id: user2Id });

          expect(user1).to.have.property('_id', user1Id);
          expect(user1).to.have.property('username', 'aerendon');
          expect(user1).to.have.property('email', 'alejorendon@utp.edu.co');

          expect(user2).to.have.property('_id', user2Id);
          expect(user2).to.have.property('username', 'leiver');
          expect(user2).to.have.property('email', 'leiverandres@utp.edu.co');
          done();
        }, done);
    });

    it('should return an array with all users pending', done => {
      let user1Id;
      let user2Id;
      const user1 = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        username: faker.internet.userName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        photo: faker.image.avatar(),
        professionalProfile: [faker.name.jobTitle()],
        status: [faker.name.jobTitle()],
        cv: faker.lorem.text
      };

      const user2 = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        username: faker.internet.userName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        photo: faker.image.avatar(),
        professionalProfile: [faker.name.jobTitle()],
        status: [faker.name.jobTitle()],
        cv: faker.lorem.text
      };

      request
        .post('/api/users')
        .send(user1)
        .set('Accept', 'application/json')
        .expect(201)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          user1Id = res.body.data._id;
          return request
            .post('/api/users')
            .send(user2)
            .set('Accept', 'application/json')
            .expect(201)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          user2Id = res.body.data._id;
          return request
            .get('/api/users?status=pending')
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          const { body } = res;
          expect(body).to.have.property('success', true);
          expect(body).to.have.property('data');
          expect(body.data).to.be.an('array');

          const { data } = body;
          const usr1 = _.find(data, { _id: user1Id });
          const usr2 = _.find(data, { _id: user2Id });

          expect(usr1).to.have.property('_id', user1Id);
          expect(usr1).to.have.property('username', user1.username);
          expect(usr1).to.have.property('email', user1.email);

          expect(usr2).to.have.property('_id', user2Id);
          expect(usr2).to.have.property('username', user2.username);
          expect(usr2).to.have.property('email', user2.email);
          done();
        }, done);
    });

    it('should return an array with all admins registered', done => {
      let user1Id;
      let user2Id;
      const user1 = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        username: faker.internet.userName(),
        email: faker.internet.email(),
        privilegeType: 1,
        password: faker.internet.password(),
        photo: faker.image.avatar(),
        professionalProfile: [faker.name.jobTitle()],
        status: [faker.name.jobTitle()],
        cv: faker.lorem.text
      };

      const user2 = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        username: faker.internet.userName(),
        email: faker.internet.email(),
        privilegeType: 1,
        password: faker.internet.password(),
        photo: faker.image.avatar(),
        professionalProfile: [faker.name.jobTitle()],
        status: [faker.name.jobTitle()],
        cv: faker.lorem.text
      };

      request
        .post('/api/users')
        .send(user1)
        .set('Accept', 'application/json')
        .expect(201)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          user1Id = res.body.data._id;
          return request
            .post('/api/users')
            .send(user2)
            .set('Accept', 'application/json')
            .expect(201)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          user2Id = res.body.data._id;
          return request
            .get('/api/users?rol=admin')
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          const { body } = res;
          expect(body).to.have.property('success', true);
          expect(body).to.have.property('data');
          expect(body.data).to.be.an('array');

          const { data } = body;
          const usr1 = _.find(data, { _id: user1Id });
          const usr2 = _.find(data, { _id: user2Id });

          expect(usr1).to.have.property('_id', user1Id);
          expect(usr1).to.have.property('username', user1.username);
          expect(usr1).to.have.property('email', user1.email);

          expect(usr2).to.have.property('_id', user2Id);
          expect(usr2).to.have.property('username', user2.username);
          expect(usr2).to.have.property('email', user2.email);
          done();
        }, done);
    });
  });

  describe('GET /api/users/:username', () => {
    it('should return the data of a user with a specific username', done => {
      const user = {
        firstName: 'Sebastian',
        lastName: 'Duque Restrepo',
        username: 'sebasd95',
        email: 'sebas_tian_95@hotmail.com',
        privilegeType: 1,
        password: 'sebas456',
        photo: '/',
        professionalProfile: ['Estudiante'],
        status: ['Monitor']
      };

      request
        .post('/api/users')
        .send(user)
        .set('Accept', 'application/json')
        .expect(201)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          const { username } = res.body.data;
          return request
            .get('/api/users/' + username)
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          const { body } = res;
          const { data } = body;
          expect(body).to.have.property('success', true);
          expect(body).to.have.property('data');
          expect(data).to.have.property('firstName', 'Sebastian');
          expect(data).to.have.property('username', 'sebasd95');
          done();
        }, done);
    });
  });

  describe('POST /api/users', () => {
    it('should return an object with the user inserted', done => {
      const user = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        username: faker.internet.userName(),
        email: faker.internet.email(),
        password: faker.internet.password(),
        photo: faker.image.avatar(),
        professionalProfile: [faker.name.jobTitle()],
        status: [faker.name.jobTitle()],
        cv: faker.lorem.text
      };

      request
        .post('/api/users')
        .send(user)
        .set('Accept', 'application/json')
        .expect(201)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          const { data } = res.body;
          expect(data).to.have.property('firstName');
          done();
        }, done);
    });
  });

  describe('PUT /api/users/:username', () => {
    it('Return the data updated of a user with a specific username', done => {
      const user = {
        firstName: 'Leiver Andres',
        lastName: 'Campeon',
        username: 'leiverandres',
        email: 'leiverandres094p@hotmail.com',
        password: 'leiver456',
        photo: '/',
        professionalProfile: ['Estudiante'],
        status: ['Monitor']
      };

      request
        .post('/api/users')
        .send(user)
        .set('Accept', 'application/json')
        .expect(201)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          let user = res.body.data;
          const username = user.username;
          user.username = 'toño';
          user.lastName = 'Benjumea';
          return request
            .put('/api/users/' + username)
            .send(user)
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          const { body } = res;
          const { data } = body;
          expect(body).to.have.property('data');
          expect(body).to.have.property('success', true);
          expect(data).to.have.property('lastName', 'Benjumea');
          expect(data).to.have.property('username', 'toño');
          expect(data).to.have.property(
            'email',
            'leiverandres094p@hotmail.com'
          );
          done();
        }, done);
    });
  });

  describe('DELETE /api/users/:username', () => {
    it('should remove an specific user', done => {
      const user = {
        firstName: 'Carolina',
        lastName: 'Gomez Trejos',
        username: 'carogt',
        email: 'carolina9511@gmail.com',
        privilegeType: 1,
        password: 'caro123',
        photo: '/',
        professionalProfile: ['Estudiante'],
        status: ['Monitor']
      };

      request
        .post('/api/users')
        .send(user)
        .set('Accept', 'application/json')
        .expect(201)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          const { username } = res.body.data;
          return request
            .delete('/api/users/' + username)
            .set('Accept', 'application/json')
            .expect(204);
        }, done)
        .then(res => {
          const { body } = res;
          expect(body).to.be.empty;
          done();
        }, done);
    });
  });

  describe('GET /api/users/:username/projects', () => {
    it('Return an array with all projects of a specific user', done => {
      let project1Id;
      let project2Id;
      let username;
      const user = {
        firstName: 'Carolina',
        lastName: 'Gomez Trejos',
        username: 'carogomezt',
        email: 'caro_gomezt@utp.edu.co',
        privilegeType: 1,
        password: 'caro789',
        photo: '/',
        professionalProfile: ['Estudiante'],
        status: ['Monitor']
      };

      const project1 = {
        name: 'LIMS',
        type: ['Investigativo', 'Ingesoft'],
        status: 'Pendiente',
        stakeholders: {
          registered: ['carogomezt', 'sebas095']
        },
        image: '/',
        summary: 'En proceso',
        report: 'pdf'
      };

      const project2 = {
        name: 'Polymer y Web Components',
        type: ['Desarrollo Web'],
        status: 'En proceso',
        stakeholders: {
          registered: ['carogomezt', 'aerendon']
        },
        image: '/',
        summary: 'Libreria de Google',
        report: 'pdf'
      };

      request
        .post('/api/users')
        .send(user)
        .set('Accept', 'application/json')
        .expect(201)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          username = res.body.data.username;
          return request
            .post('/api/projects')
            .send(project1)
            .set('Accept', 'application/json')
            .expect(201)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          project1Id = res.body.data._id;
          return request
            .post('/api/projects')
            .send(project2)
            .set('Accept', 'application/json')
            .expect(201)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          project2Id = res.body.data._id;
          return request
            .get('/api/users/' + username + '/projects')
            .set('Accept', 'application/json')
            .expect(200)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          const { body } = res;
          expect(body).to.have.property('success', true);
          expect(body).to.have.property('data');
          expect(body.data).to.be.an('array');

          const { data } = body;
          const project1 = _.find(data, { _id: project1Id });
          const project2 = _.find(data, { _id: project2Id });

          expect(project1).to.have.property('_id', project1Id);
          expect(project1).to.have.property('name', 'LIMS');
          expect(project1).to.have.property('status', 'Pendiente');
          expect(project1).to.have.property('type');
          expect(project1.type).to.be.an('array');

          expect(project2).to.have.property('_id', project2Id);
          expect(project2).to.have.property('name', 'Polymer y Web Components');
          expect(project2).to.have.property('status', 'En proceso');
          expect(project2).to.have.property('type');
          expect(project2.type).to.be.an('array');
          done();
        }, done);
    });
  });
});
