const faker = require('faker');
const mongoose = require('mongoose');

const config = require('../config/test');
const User = require('../models/user');

mongoose.Promise = global.Promise;

describe('User model', () => {
  before('Connecting the db', done => {
    mongoose.connect(config.db.url);
    mongoose.connection.once('open', () => done()).on('error', error => {
      console.warn('Error:', error);
      done(err);
    });
  });

  after('Drop user collection', done => {
    mongoose.connection.collections.users.drop(() => {
      done();
    });
  });

  it('Should validate if mandatory field are valid', done => {
    const userData = {
      username: faker.internet.userName(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      password: faker.internet.password(),
      email: faker.internet.email()
    };

    const newUser = new User(userData);
    newUser.save(err => {
      expect(err).to.not.exist;
      done();
    });
  });

  it('Should detect missing require fields', done => {
    const newUser = new User();
    newUser.save(err => {
      expect(err.errors.firstName).to.exist;
      expect(err.errors.lastName).to.exist;
      expect(err.errors.username).to.exist;
      expect(err.errors.password).to.exist;
      expect(err.errors.email).to.exist;
      done();
    });
  });

  it('Should create user with all fields', done => {
    const userData = {
      username: faker.internet.userName(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      password: faker.internet.password(),
      email: faker.internet.email(),
      profession: faker.name.jobTitle(),
      phoneNumber: '3229371',
      website: faker.internet.url(),
      cv: faker.image.imageUrl(), // imagin is a pdf
      picture: faker.image.imageUrl(),
      role: faker.name.jobTitle(),
      socialNetworks: {
        facebook: faker.internet.url(),
        twitter: faker.internet.url(),
        linkedin: faker.internet.url(),
        github: faker.internet.url(),
        youtube: faker.internet.url()
      },
      description: faker.lorem.paragraph(),
      education: [
        {
          title: faker.name.jobArea(),
          institute: faker.company.companyName(),
          startYear: 2000,
          endYear: 20002,
          description: faker.name.jobDescriptor()
        },
        {
          title: faker.name.jobArea(),
          institute: faker.company.companyName(),
          startYear: 2003,
          endYear: 20005,
          description: faker.name.jobDescriptor()
        }
      ],
      papers: [
        {
          date: faker.date.past(2),
          title: faker.lorem.sentence(),
          publisher: faker.company.companyName(),
          url: faker.internet.url()
        }
      ],
      skills: [
        {
          name: 'Python',
          level: 4
        }
      ]
    };
    console.log(userData);
    const newUser = new User(userData);
    newUser.save(err => {
      expect(err).to.not.exist;
      done();
    });
  });

  it('Should compare correctly the password', done => {
    const password = faker.internet.password();
    const userData = {
      username: faker.internet.userName(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      password,
      email: faker.internet.email()
    };

    User.create(userData)
      .then(newUser => {
        newUser
          .comparePassword(password)
          .then(isMatch => {
            expect(isMatch).to.equal(true);
            done();
          })
          .catch(err => {
            done(err);
          });
      })
      .catch(err => {
        done(err);
      });
  });

  it('Should catch a incorrect password', done => {
    const password = faker.internet.password();
    const anotherPassword = faker.internet.password();
    const userData = {
      username: faker.internet.userName(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      password,
      email: faker.internet.email()
    };

    User.create(userData)
      .then(newUser => {
        newUser
          .comparePassword(anotherPassword)
          .then(isMatch => {
            expect(isMatch).to.equal(false);
            done();
          })
          .catch(err => {
            done(err);
          });
      })
      .catch(err => {
        done(err);
      });
  });
});
