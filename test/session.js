let request = require('supertest-as-promised');
const api = require('../app');
const host = api.server; // podemos pasarlo otra url
const mongoose = require('mongoose');
const config = require('../config/test');
request = request(host);

describe('Session route', () => {
  before(() => {
    mongoose.Promise = global.Promise;
    mongoose.connect(config.db.url);
  });

  after(done => {
    mongoose.disconnect(done);
    mongoose.models = {};
  });

  describe('GET /api/session/login', () => {
    it('should return ok', done => {
      request
        .get('/api/session/login')
        .set('Accept', 'application/json')
        .expect(200)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          const { body } = res;
          expect(body).to.have.property('message', 'ok');
          done();
        }, done);
    });
  });

  describe('POST /api/session/login', () => {
    it('should return token', done => {
      const user = {
        firstName: 'Sebastian',
        lastName: 'Duque Restrepo',
        username: 'sebas095',
        email: 'sebas.duque@utp.edu.co',
        privilegeType: 1,
        password: 'sebas123',
        photo: '/',
        professionalProfile: ['Estudiante'],
        status: ['Monitor']
      };

      request
        .post('/api/users')
        .send(user)
        .set('Accept', 'application/json')
        .expect(201)
        .expect('Content-Type', /application\/json/)
        .then(res => {
          const _user = {
            username: res.body.data.username,
            password: 'sebas123'
          };
          return request
            .post('/api/session/login')
            .send(_user)
            .set('Accept', 'application/json')
            .expect(201)
            .expect('Content-Type', /application\/json/);
        }, done)
        .then(res => {
          const { body } = res;
          expect(body).to.have.property('success', true);
          expect(body).to.have.property('token');
          done();
        }, done);
    });
  });
});
