# SiriusWebProject-Backend

[![Build Status](https://travis-ci.org/labsirius/SiriusWebProject-Backend.svg?branch=master)](https://travis-ci.org/labsirius/SiriusWebProject-Backend)
[![dependencies Status](https://david-dm.org/labsirius/SiriusWebProject-Backend/status.svg)](https://david-dm.org/labsirius/SiriusWebProject-Backend)
[![Github Issues](https://img.shields.io/github/issues/labsirius/SiriusWebProject-Backend.svg)](http://github.com/labsirius/SiriusWebProject-Backend/issues)

## Requeriments/dependencies
* [NodeJS](https://nodejs.org/en/)
* [Mongodb](https://docs.mongodb.com/manual/installation/)

## Installation
```bash
npm install
```
You must run the database first for example:
```bash
mongod --dbpath ./data
```

## Testing
```bash
npm run test
```

## Run
```bash
npm start
```

## RESTful API
| url                     | method   | description                | params |
| ----------------------- | -------- | -------------------------- | ------------- |
| /api/users              | GET      | returns all the users      |               |
| /api/users              | POST     | creates a new user         | <ul><li>firstName</li><li>lastName</li><li>username</li><li>email</li><li>password</li><li>cv</li><li>status</li><li>professionalProfile</li><li>photo</li><li>privilegeType</li></ul> |
| /api/users/:username    | GET      | return a specific user     |               |
| /api/users/:username    | PUT      | modifies user data         | <ul><li>firstName</li><li>lastName</li><li>username</li><li>email</li><li>password</li><li>privilegeType</li><li>cv</li><li>status</li><li>professionalProfile</li><li>photo</li></ul> |
| /api/users/:username    | DELETE   | remove a user (deleted logical) |               |
| /api/users/:username/projects | GET | return all projects of a user  |         |
| /api/session/login      | POST     | create a new session and return a token   | <ul><li>username</li><li>password</li></ul>  |
| /api/projects           | GET      | returns all the projects   |               |
| /api/projects           | POST     | creates a new project      | <ul><li>name</li><li>type</li><li>status</li><li>stakeholders</li><li>image</li><li>summary</li><li>report</li></ul> |
| /api/projects/:projectId | GET     | return a specific project  |               |
| /api/projects/:projectId | PUT     | modifies project data      |               |
| /api/projects/:projectId/users | GET  | return all users of a project |          |
| /api/projects/:projectId | DELETE  | remove a project           |               |

## Note
For the storage service will be used [SiriusStorage](https://github.com/labsirius/SiriusStorage)

## Using SiriusWebProject-Backend from command line
The SiriusWebProject-Backend can be used with [curl](https://en.wikipedia.org/wiki/CURL), here you have some examples.
We strongly recommend you to use [prettyJSON](https://www.npmjs.com/package/prettyjson)

* Create a new user:
```bash
$ curl -X POST -H 'Content-Type: application/json' -d '{"firstName": "Sirius","lastName": "HPC", "username": "sirius", "email": "sirius@gmail.com", "password": "sirius123", "professionalProfile": ["Investigador"], "status": "Empresario"}' http://localhost:4000/api/users | prettyjson http://localhost:4000/api/session/login
```
You will receive the following:
```bash
__v:                 0
firstName:           Sirius
username:            sirius
lastName:            HPC
email:               sirius@gmail.com
_id:                 58439787fa065e7a50bd9220
cv:
status:
  - Empresario
professionalProfile:
  - Investigador
photo:
privilegeType:       0
```

### Configuración

* [EditorConfig](http://editorconfig.org/)

___
<p align="center"><img src="http://forthebadge.com/images/badges/made-with-crayons.svg"></img></p>
<p align="center"><img src="http://forthebadge.com/images/badges/built-by-developers.svg"></img></p>
<a href="http://sirius.utp.edu.co" target="_blank"><p align="center"><img src="https://github.com/jointDeveloper/web/blob/master/public/IMG/icons/sirius.png?raw=true" width=110px></img></p></a>
