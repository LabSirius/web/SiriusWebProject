const accountStatus = {
  PENDING: 'PENDING',
  ADMIN: 'ADMIN',
  APPROVED: 'APPROVED',
  DISABLED: 'DISABLED'
};

module.exports = { accountStatus };
