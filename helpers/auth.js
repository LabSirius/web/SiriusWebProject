const passport = require('passport');
const env = process.env.NODE_ENV || 'dev';
const config = require('../config/' + env);
const { ExtractJwt, Strategy } = require('passport-jwt');
const User = require('../models/user');

// Configure Strategy
const params = {
  secretOrKey: config.secret,
  jwtFromRequest: ExtractJwt.fromAuthHeader()
};

module.exports = () => {
  const strategy = new Strategy(params, (payload, done) => {
    User.findOne({ _id: payload.sub })
      .then(user => {
        if (!user) {
          return done(new Error('User not found'), null);
        } else return done(null, { id: user._id });
      })
      .catch(err => {
        return done(new Error('User not found'), null);
      });
  });

  passport.use(strategy);
  return {
    initialize: () => passport.initialize(),
    authenticate: () => passport.authenticate('jwt', { session: false })
  };
};
