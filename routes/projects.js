/* ROUTES projects. */
const { Router } = require('restify-router');
const projectRouter = new Router();
const Project = require('../models/project');

module.exports = (app, mountPoint, auth) => {
  projectRouter.get(`${mountPoint}/`, (req, res) => {
    Project.find()
      .then(projects => {
        res.send(200, { success: true, data: projects });
      })
      .catch(err => {
        res.send(500, { success: false, message: 'Problem getting projects' });
      });
  });

  projectRouter.post(`${mountPoint}/`, (req, res) => {
    if (!req.body) {
      res.send(403, { success: false, message: 'Body empty' });
    }
    // Creates a new project
    const project = new Project(req.body);
    Project.findOne({
      name: project.name,
      type: project.type,
      status: project.status,
      stakeholders: project.stakeholders
    })
      .then(doc => {
        if (doc) {
          return Promise.resolve(doc);
        } else {
          return project.save();
        }
      })
      .then(proj => {
        res.send(201, { success: true, data: proj });
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem creating project' });
      });
  });

  projectRouter.get(`${mountPoint}/:projectId/`, (req, res) => {
    // Return a project with specific id
    Project.findById(req.params.projectId)
      .then(project => {
        if (project) {
          res.send(200, { success: true, message: 'ok', data: project });
        } else {
          res.send(404, { success: false, message: 'No project with such id' });
        }
      })
      .catch(err => {
        res.send(500, { success: false, message: 'Problem getting project' });
      });
  });

  projectRouter.put(`${mountPoint}/:projectId/`, (req, res) => {
    // Updates an specific project
    Project.findOneAndUpdate({ _id: req.params.projectId }, req.body, {
      new: true
    })
      .then(project => {
        res.send(200, { success: true, data: project });
      })
      .catch(err => {
        res.send(500, { success: false, message: 'Problem updating project' });
      });
  });

  projectRouter.del(`${mountPoint}/:projectId/`, (req, res) => {
    // Eliminates a project with specific id
    Project.findByIdAndRemove(req.params.projectId)
      .then(project => {
        if (project) {
          res.send(400, { success: true, data: {} });
        } else {
          res.send(404, { success: false, message: 'No project with such id' });
        }
      })
      .catch(err => {
        res.send(500, { success: false, message: 'Problem deleting project' });
      });
  });

  projectRouter.get(`${mountPoint}/:projectId/users/`, (req, res) => {
    // Return all Stakeholders from project
    console.log(req.params.projectId);
    if (req.params.projectId) {
      Project.findById(req.params.projectId, 'stakeholders')
        .then(data => {
          if (data) {
            res.send(200, { success: true, data: data.stakeholders });
          } else {
            res.send(404, {
              success: false,
              message: 'No project with such id'
            });
          }
        })
        .catch(err => {
          res.send(500, { success: false, message: 'Problem getting project' });
        });
    } else {
      res.send(403, { success: false, message: 'Params empty' });
    }
  });

  projectRouter.applyRoutes(app.server);
};
