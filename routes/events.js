const { Router } = require('restify-router');
const Event = require('../models/event');
const eventRouter = new Router();

module.exports = (app, mountPoint, auth) => {
  eventRouter.get(`${mountPoint}/`, (req, res) => {
    Event.find()
      .then(events => {
        res.send(200, { success: true, data: events });
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem getting events' });
      });
  });

  eventRouter.get(`${mountPoint}/:id/`, (req, res) => {
    if (!req.params.id) {
      res.send(403, { success: false, message: 'Params empty' });
    }
    Event.findById(req.params.id)
      .then(event => {
        res.send(200, { success: true, data: event });
      })
      .catch(err => {
        res.send(404, { success: false, message: 'Event not found' });
      });
  });

  eventRouter.post(`${mountPoint}/`, auth.authenticate(), (req, res) => {
    if (!req.body) {
      res.send(403, { success: false, message: 'Body empty' });
    }
    Event.create(req.body)
      .then(event => {
        res.send(201, { success: true, data: event });
      })
      .catch(err => {
        console.error(err);
        res.send(500, {
          success: false,
          message: 'Problem creating the event'
        });
      });
  });

  eventRouter.put(`${mountPoint}/:id/`, auth.authenticate(), (req, res) => {
    if (!req.params.id || !req.body) {
      res.send(403, { success: false, message: 'Params/body empty' });
    } else {
      Event.findByIdAndUpdate(req.params.id, req.body, {
        new: true
      })
        .then(event => {
          res.send(200, { success: true, data: event });
        })
        .catch(err => {
          res.send(500, { success: false, message: 'Internal server problem' });
        });
    }
  });

  eventRouter.del(`${mountPoint}/:id/`, auth.authenticate(), (req, res) => {
    if (!req.params.id) {
      res.send(403, { success: false, message: 'Params empty' });
    }
    Event.findByIdAndRemove(req.params.id)
      .then(event => {
        if (event) {
          res.send(200, { success: true, data: {} });
        } else {
          res.send(404, { success: false, message: 'No event with such id' });
        }
      })
      .catch(err => {
        res.send(500, { success: false, message: 'Problem deleting event' });
      });
  });

  eventRouter.applyRoutes(app.server);
};
