/* ROUTES sessions. */
const { Router } = require('restify-router');
const User = require('../models/user');
const jwt = require('jwt-simple');
const env = process.env.NODE_ENV || 'dev';
const config = require('../config/' + env);
const moment = require('moment');
const sessionRouter = new Router();

module.exports = (app, mountPoint) => {
  sessionRouter.get(`${mountPoint}/login/`, (req, res) => {
    res.send(200, { success: true, message: 'ok' });
  });

  sessionRouter.post(`${mountPoint}/login/`, (req, res) => {
    let usr = null;
    User.findOne({ username: req.body.username })
      .then(user => {
        if (!user) {
          return Promise.resolve(false);
        } else {
          usr = user;
          return user.comparePassword(req.body.password);
        }
      })
      .then(isMatch => {
        if (isMatch) {
          const payload = {
            sub: usr._id,
            iat: Date.now(),
            exp: Date.now() + 8 * 60 * 60 * 1000
          };
          const token = jwt.encode(payload, config.secret);
          res.send(201, {
            success: true,
            token: 'JWT ' + token,
            data: usr.username,
            valid: usr.privilegeType === '1'
          });
        } else {
          res.send(401, {
            success: false,
            message: 'Login authentication failed'
          });
        }
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Server problem' });
      });
  });

  sessionRouter.applyRoutes(app.server);
};
