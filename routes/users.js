/* ROUTES users. */
const { Router } = require('restify-router');
const nodemailer = require('nodemailer');
const crypto = require('crypto');
const bcrypt = require('bcrypt');

const { auth } = require('../config/email');
const { accountStatus } = require('../helpers/constants');
const User = require('../models/user');
const Project = require('../models/project');

const userRouter = new Router();
const transporter = nodemailer.createTransport(
  `smtps://${auth.user}:${auth.pass}@smtp.gmail.com`
);

module.exports = (app, mountPoint, auth) => {
  userRouter.get(`${mountPoint}/`, (req, res) => {
    User.find()
      .then(users => {
        res.send(200, { success: true, data: users });
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem getting users' });
      });
  });

  userRouter.get(`${mountPoint}/auth`, auth.authenticate(), (req, res) => {
    User.find()
      .then(users => {
        if (req.query.status === 'pending') {
          users = users.filter(user => user.privilegeType === '0');
        } else if (req.query.rol === 'admin') {
          users = users.filter(user => user.privilegeType === '1');
        } else {
          users = users.filter(user => user.privilegeType !== '0');
        }
        res.send(200, { success: true, data: users });
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem getting users' });
      });
  });

  /**
   * Creates a new user
   */
  userRouter.post(`${mountPoint}/`, (req, res) => {
    let user;

    if (!req.body) {
      res.send(403, { success: false, message: 'Body empty' });
    }

    // TODO: find another way to initialize super user
    User.find()
      .then(users => {
        if (!req.body.accountStatus) {
          if (users.length === 0) {
            // first user in the system will be assigned as ADMIN
            Object.assign(req.body, { accountStatus: accountStatus.ADMIN });
          }
        }
        // TODO: ensures just admin user modify accountStatus field
        user = new User(req.body);
        return User.findOne({ username: user.username });
      })
      .then(doc => {
        if (doc) {
          return Promise.resolve(doc);
        }
        return user.save();
      })
      .then(usr => {
        res.send(201, { success: true, data: usr });
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem creating the user' });
      });
  });

  /**
   * Retrieve a specific user by username
   */
  userRouter.get(`${mountPoint}/:username/`, (req, res) => {
    User.findOne({ username: req.params.username })
      .then(user => {
        res.send(200, { success: true, data: user });
      })
      .catch(err => {
        console.error(err);
        res.send(404, { success: false, message: 'User not found' });
      });
  });

  userRouter.put(
    // `${mountPoint}/:username/`,
    // auth.authenticate(),
    // (req, res) => {
    `${mountPoint}/:username/`,
    (req, res) => {
      if (!req.params.username || !req.body) {
        res.send(403, { success: false, message: 'Params/body empty' });
      } else {
        User.findOneAndUpdate(
          {
            username: req.params.username
          },
          req.body,
          { new: true }
        )
          .then(user => {
            res.send(200, { success: true, data: user });
          })
          .catch(err => {
            res.send(500, {
              success: false,
              message: 'Internal server problem'
            });
          });
      }
    }
  );

  userRouter.del(
    `${mountPoint}/:username/`,
    auth.authenticate(),
    (req, res) => {
      if (!req.params.username) {
        res.send(403, { success: false, message: 'Params empty' });
      }
      // Eliminates a user with specific username
      User.update({ username: req.params.username }, { privilegeType: 3 })
        .then(user => {
          res.send(204);
        })
        .catch(err => {
          res.send(500, { success: false, message: 'Internal server problem' });
        });
    }
  );

  userRouter.get(`${mountPoint}/:username/projects/`, (req, res) => {
    // Return all project from a user
    const projects = [];
    if (!req.params.username) {
      res.send(403, { success: false, message: 'Params empty' });
    }

    const { username } = req.params;
    Project.find()
      .then(data => {
        for (const index in data) {
          if (data[index].stakeholders.registered.indexOf(username) !== -1) {
            projects.push(data[index]);
          }
        }
        res.send(200, { success: true, data: projects });
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem getting projects' });
      });
  });

  userRouter.get(`${mountPoint}/resetPassword/:token/`, (req, res) => {
    User.findOne({
      resetPasswordToken: req.params.token,
      resetPasswordExpires: { $gt: Date.now() }
    })
      .then(user => {
        if (user) {
          res.send(200, {
            success: true,
            message: 'ok',
            data: req.params.token
          });
        } else {
          res.send(404, { success: false, message: 'User not found' });
        }
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem getting user' });
      });
  });

  userRouter.post(`${mountPoint}/emailRecovery/`, (req, res) => {
    if (!req.body) {
      res.send(403, { success: false, message: 'Body empty' });
    } else {
      const { email } = req.body;
      crypto.randomBytes(30, (err, buff) => {
        if (err) {
          return res.send(500, {
            success: false,
            message: 'Internal server problem'
          });
        }
        const token = buff.toString('hex');
        User.findOne({ email })
          .then(user => {
            if (!user) {
              res.send(404, { success: false, message: 'User not found' });
            } else {
              user.resetPasswordToken = token;
              user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
              user.save(err => {
                if (err) {
                  res.send(500, {
                    success: false,
                    message: 'Internal server problem'
                  });
                } else {
                  const mailOptions = {
                    from: 'Administración',
                    to: user.email,
                    subject: 'Recuperación de contraseña',
                    html: `<p>Estimado Usuario ${user.firstName} ${user.lastName},</p><br>
                      Para una nueva contraseña de clic al siguiente enlace:
                      <br><a href="${HOST}/users/reset/${token}">
                      Recuperar Contraseña</a><br><br>Si usted no lo solicitó, ignore
                      este correo electrónico y su contraseña permanecerá sin cambios.
                      <br><br><br>Att,<br><br>Equipo Administrativo`
                  };

                  transporter.sendMail(mailOptions, err => {
                    if (err) {
                      console.log(err);
                      res.send(500, {
                        success: false,
                        message: 'Internal server problem'
                      });
                    } else {
                      res.send(200, {
                        success: true,
                        message: 'ok'
                      });
                    }
                  });
                }
              });
            }
          })
          .catch(err => {
            console.error(err);
            res.send(500, { success: false, message: 'Problem getting user' });
          });
      });
    }
  });

  userRouter.put(`${mountPoint}/reset/:token/`, (req, res) => {
    if (!req.params || !req.body) {
      res.send(403, { success: false, message: 'Params/body empty' });
    } else {
      const { token } = req.params;
      if (req.body.password !== req.body.cpassword) {
        res.send(400, { success: false, message: 'Passwords do not match' });
      } else {
        User.findOne({
          resetPasswordToken: token,
          resetPasswordExpires: { $gt: Date.now() }
        })
          .then(user => user.comparePassword(req.body.password))
          .then(isMatch => {
            if (isMatch) {
              res.send(500, {
                success: false,
                message:
                  'That password was the one you had previously, ' +
                  'please try again with another password'
              });
            } else {
              bcrypt.genSalt(10, (err, salt) => {
                if (err) {
                  console.log(err);
                  return res.send(500, {
                    success: false,
                    message: 'Internal server problem'
                  });
                }
                bcrypt
                  .hash(req.body.password, salt)
                  .then(hash =>
                    User.findOneAndUpdate(
                      {
                        resetPasswordToken: token,
                        resetPasswordExpires: { $gt: Date.now() }
                      },
                      { password: hash },
                      { new: true }
                    )
                  )
                  .then(usr => {
                    res.send(200, { success: true, message: 'ok', data: usr });
                  })
                  .catch(err => {
                    console.error(err);
                    res.send(500, {
                      success: false,
                      message: 'Problem getting user'
                    });
                  });
              });
            }
          })
          .catch(err => {
            console.error(err);
            res.send(500, { success: false, message: 'Problem getting user' });
          });
      }
    }
  });

  userRouter.applyRoutes(app.server);
};
