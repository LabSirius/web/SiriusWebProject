const { Router } = require('restify-router');

const News = require('../models/news');

const newsRouter = new Router();

/**
 * Sort query param:
 * Its value will be the field to take as sorting condition.
 * if the value is prefixed with a '-' means that sorting must be descending,
 * ascending otherwise.
 * For example:
 * ?sort=-date 	Sort via date field in descending order
 * ?sort=date 	Sort via date field in ascending order
 * ?sort=-date,amount Sort via date field in descending order and via amount in
 *                    ascending order
 *
 */

module.exports = (app, mountPoint, auth) => {
  newsRouter.get(`${mountPoint}/`, (req, res) => {
    const sortQuery = { date: 'desc' }; // default sorting parameter
    if (req.params.sort) {
      const sortingParams = req.params.sort.split(',');
      for (let sortParam of sortingParams) {
        if (sortParam.startsWith('-')) {
          sortQuery[sortParam.split('-')[1]] = 'desc';
        } else {
          sortQuery[sortParam] = 'asc';
        }
      }
    }

    News.find()
      .sort(sortQuery)
      .then(news => {
        res.send(200, { success: true, data: news });
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem getting all news' });
      });
  });

  newsRouter.post(`${mountPoint}/`, (req, res) => {
    if (!req.body) {
      res.send(403, { success: false, message: 'Body empty' });
    }

    const newNew = new News(req.body);

    News.findOne({
      title: newNew.title,
      authors: newNew.authors,
      body: newNew.body
    })
      .then(doc => {
        if (doc) {
          res.send(409, {
            success: false,
            message: 'There is already a New with same title, body and authors',
            data: doc
          });
        } else {
          return newNew.save();
        }
      })
      .then(newAdded => {
        res.send(201, { success: true, data: newAdded });
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem saving new' });
      });
  });

  newsRouter.get(`${mountPoint}/:newId/`, (req, res) => {
    News.findById(req.params.newId)
      .then(newFound => {
        if (newFound) {
          res.send(200, { success: true, message: 'ok', data: newFound });
        } else {
          res.send(404, { success: false, message: 'No new with such id' });
        }
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem getting new' });
      });
  });

  newsRouter.put(`${mountPoint}/:newId/`, (req, res) => {
    News.findOneAndUpdate({ _id: req.params.newId }, req.body, { new: true })
      .then(newFound => {
        res.send(200, { success: true, data: newFound });
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem updating new' });
      });
  });

  newsRouter.del(`${mountPoint}/:newId/`, (req, res) => {
    News.findByIdAndRemove(req.params.newId)
      .then(newRemoved => {
        if (newRemoved) {
          res.send(204, { success: true });
        } else {
          res.send(404, { success: false, message: 'No new with such id' });
        }
      })
      .catch(err => {
        console.error(err);
        res.send(500, { success: false, message: 'Problem deleting new' });
      });
  });

  newsRouter.applyRoutes(app.server);
};
