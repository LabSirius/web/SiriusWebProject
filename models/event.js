const mongoose = require('mongoose');
const timestamp = require('mongoose-timestamp');

const Schema = mongoose.Schema;

const EventSchema = new Schema({
  name: {
    type: String,
    require: true
  },
  startDate: {
    type: Date,
    require: true
  },
  finishDate: {
    type: Date,
    require: true
  },
  description: {
    type: String,
    require: true,
    default: ''
  },
  author: {
    type: String,
    require: true
  },
  createdBy: {
    type: Object,
    require: true,
    default: {
      registered: [],
      others: []
    }
  },
  img: {
    type: String,
    default: ''
  },
  tags: {
    type: Array,
    default: []
  },
  place: {
    type: String,
    default: ''
  },
  resources: {
    type: Array,
    require: true
  }
});

EventSchema.plugin(timestamp);
module.exports = mongoose.model('Event', EventSchema);
