const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const { accountStatus } = require('../helpers/constants');

const Schema = mongoose.Schema;
const SALT_WORK_FACTOR = 10;

const UserSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
    trim: true,
    lowercase: true,
    match: /^[\w-.]+$/
  },
  firstName: {
    type: String,
    required: true,
    trim: true,
    match: /(?=^.{2,}$)[A-Za-zñÑáÁéÉíÍóÓúÚüÜ ]+/
  },
  password: {
    type: String,
    required: true,
    trim: true,
    match: /^[\w-ñÑáÁéÉíÍóÓúÚüÜ!#$%&/]{6,30}$/
  },
  lastName: {
    type: String,
    required: true,
    trim: true,
    match: /(?=^.{2,}$)[A-Za-zñÑáÁéÉíÍóÓúÚüÜ ]+/
  },
  email: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
    trim: true,
    match: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
  },
  profession: { type: String, trim: true },
  phoneNumber: { type: String, trim: true, match: /\d{7,15}/ },
  website: { type: String, trim: true },
  cv: { type: String, trim: true },
  picture: { type: String, trim: true },
  role: { type: String, trim: true },
  socialNetworks: {
    facebook: { type: String, trim: true },
    twitter: { type: String, trim: true },
    linkedin: { type: String, trim: true },
    github: { type: String, trim: true },
    youtube: { type: String, trim: true }
  },
  description: { type: String, trim: true },
  education: [
    {
      title: { type: String, required: true },
      institute: { type: String, required: true },
      startYear: { type: Number, required: true, min: 1940 },
      endYear: { type: Number, min: 1940 },
      description: { type: String, required: true }
    }
  ],
  papers: [
    {
      date: { type: Date, required: true, min: new Date(1940, 1, 1) },
      title: { type: String, required: true },
      publisher: { type: String, required: true },
      description: { type: String, required: true },
      url: { type: String, required: true }
    }
  ],
  skills: [
    {
      name: { type: String, required: true },
      level: { type: Number, required: true, min: 1, max: 100 }
    }
  ],
  accountStatus: {
    type: String,
    enum: Object.values(accountStatus), // pending, admin, user ok, user disabled
    default: accountStatus.PENDING
  },
  resetPasswordToken: String,
  resetPasswordExpires: Date
});

/**
 * Before save the object ecprypt the password
 */
UserSchema.pre('save', function preSave(next) {
  if (this.isModified('password') || this.isNew) {
    bcrypt
      .hash(this.password, SALT_WORK_FACTOR)
      .then(hash => {
        this.password = hash;
        next();
      })
      .catch(err => next(err));
  } else {
    next();
  }
});

UserSchema.methods.comparePassword = function comparePassword(
  candidatePassword
) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
      if (err) {
        return reject(err);
      }
      return resolve(isMatch);
    });
  });
};

module.exports = mongoose.model('Users', UserSchema);
