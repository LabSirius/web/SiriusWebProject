const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');

const Schema = mongoose.Schema;
autoIncrement.initialize(mongoose.connection);

const ProjectSchema = new Schema({
  name: {
    type: String,
    require: true
  },
  type: {
    type: Array,
    require: true
  },
  status: {
    type: String,
    require: true
  },
  stakeholders: {
    type: Object,
    require: true,
    default: {
      registered: [],
      others: []
    }
  },
  image: {
    type: String,
    default: ''
  },
  summary: {
    type: String,
    default: ''
  },
  report: {
    type: String,
    default: ''
  }
});

ProjectSchema.plugin(autoIncrement.plugin, 'Project');
module.exports = mongoose.model('Project', ProjectSchema);
